var Page = require('./page.js');

var LoginPage = Object.create(Page, {
  emailField: Page.makeElementFrom('input[name=email]'),
  passwordField: Page.makeElementFrom('input[name=password]'),
  btnLoginSubmit: Page.makeElementFrom('input[type=submit]'),
  loginForm: Page.makeElementFrom('form'),
  loginLogo: Page.makeElementFrom('img'),
  loginAgent: Page.makeElementFrom('input#AGENT_ROLE'),
  loginAdmin: Page.makeElementFrom('input#ADMIN_ROLE'),
  open: {
    value: function(baseUrl) {
      Page.open.call(this, baseUrl, '');
    },
  },

  submit: {
   value: function() {
      this.btnLoginSubmit.click();
    },
  },

  loginAs: {
    value: function(email, password, role) {
      // The form needs to wait for the app to load. For some reason
      // will not accept a submission until some time after the page
      // loads.
      // role can be 'admin' or 'agent'
      role = role || 'agent';
      browser.pause(500);

      this.emailField.setValue(email);
      this.passwordField.setValue(password);
      this.loginAgent.waitForVisible();
      this.loginAdmin.waitForVisible();

      if (role === 'admin') {
        this.loginAdmin.click();
      } else if (role === 'agent') {
        this.loginAgent.click();
      }

      this.submit();
    },
  },
});

module.exports = LoginPage;
