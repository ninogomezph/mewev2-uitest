'use strict';
const config = require('../configurationLoader');
const LoginPage = require('../pages/login');


module.exports = function () {
  var args = arguments;
  let role = args[2],
    email = args[0],
    password = args[1];
  if (args.length === 1) {
    role = args[0];
    email = config[`${role}_EMAIL`];
    password = config[`${role}_PASSWORD`];
    console.log('role is: ' +role);
  } else if (!email || !password || !role) {
    throw new Error('incomplete arguments in meweHelper.loginAs!');
  };

  LoginPage.open(config.MEWE_URL);
  LoginPage.emailField.setValue(email);
  LoginPage.passwordField.setValue(password);

  // assumption default role is AGENT
  if(role === 'ADMIN' || role === 'SUB_ADMIN' && LoginPage.loginAdmin.isVisible()){
    LoginPage.loginAdmin.click();
  }

  LoginPage.submit();
  const titleTest = role === 'ADMIN' || role === 'SUB_ADMIN' ? 'MeWe' : 'MeWe Agent';

  return assert.equal(browser.getTitle(), titleTest);
}
