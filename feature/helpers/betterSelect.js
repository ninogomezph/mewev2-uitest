module.exports = {
  open: (selector) => {
    browser.element(selector).waitForVisible(5000);
    browser.element(selector).moveToObject();
    browser.element(selector).click();
  },
  chooseOption: (text) => {
    browser.element('li*=' + text).waitForVisible(5000);
    browser.element('li*=' + text).click();
  }
}
