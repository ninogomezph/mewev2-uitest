'use strict';
const LeftMenu = require('../pages/admin/leftMenu');
const ChecklistPage = require('../pages/admin/checklist');
const path = require('path');

module.exports = (checklistCategory, checklistName, zipFileToUpload) => {
  const args = arguments;
  // Should have all the arguements;
  // Zip File to Upload should be at v2/test/feature/checklist folder

  if (!checklistCategory && !checklistName && !zipFileToUpload) {
    throw new Error('incomplete arguments in meweHelper.createChecklist. (checklistCategory, checklistName, zipFileToUpload)!');
  }

  // Navigate to Checklist Category Page
  LeftMenu.navItemChecklist.click();
  browser.waitForExist('a.btn=Create Category', 5000);

  // Check if Checklist Category Exist, if not then Create the Checklist Category
  if(!browser.element('=' + checklistCategory).isVisible())
  {
    // Create Checklist Category
    ChecklistPage.createCategoryBtn.click();
    ChecklistPage.newInputCategory.setValue(checklistCategory);
    browser.timeoutsImplicitWait(10);
    ChecklistPage.categorySaveBtn.click();
    browser.pause(1000);
    if(browser.element("#alert-holder > ul > li").isVisible()) {
      browser.element("#alert-holder > ul > li").click();
    }
  }


  // Navigate to list of Checklist under the Checklist Category
  browser.waitForExist('=' + checklistCategory, 5000);
  browser.click('=' + checklistCategory);
  browser.waitForExist('a.btn=Create Checklist', 5000);
  ChecklistPage.createChecklistButton.click();
  browser.waitForExist('div.btn-action=Save', 5000);

  // Create Checklist
  ChecklistPage.inputChecklistName.setValue(checklistName);
  ChecklistPage.saveChecklistBtn.click();
  browser.waitForExist('header=Question Editor', 5000);

  if(browser.element("#alert-holder > ul > li").isVisible()) {
    browser.element("#alert-holder > ul > li").click();
  }

  // Import Zip File
  browser.chooseFile("input[type='file'][accept='.zip']", path.join(__dirname, "..", "checklist", zipFileToUpload));
  browser.waitForExist(".cg > header > section > h5", 5000); // Wait for the First Question Group to appear.

  return assert(browser.element(".cg > header > section > h5").isVisible())
}
