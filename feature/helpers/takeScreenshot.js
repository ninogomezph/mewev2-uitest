'use strict';
const path = require('path');

module.exports = (filename) => {
  const name = filename || `screenshot-${(new Date).toString()}`
  browser.saveScreenshot(path.join(__dirname, "..", "screenshots", `${name}.png`))
}
