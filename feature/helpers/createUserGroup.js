'use strict';
const LeftMenu = require('../pages/admin/leftMenu');
const UserGroupPage = require('../pages/admin/userGroup');
const betterSelect = require('./betterSelect');

// userGroupInfo = {
//   checklist: "checklist name",
//   property: "property name",
//   user: "user email",
// }

module.exports = (userGroupInfo) => {
  // Go to User Group and create new User Group
  LeftMenu.userGroup.click();
  browser.pause(1000);
  UserGroupPage.createUserGroupBtn.click();
  UserGroupPage.inputUserGroupName.waitForVisible(5000);
  UserGroupPage.inputUserGroupName.setValue(userGroupInfo.name);
  UserGroupPage.saveBtn.click();
  browser.element("button='Search Users'").waitForVisible(5000);

  // Add Agent to User Group
  if(userGroupInfo.user) {
    meweHelper.multiSelect.chooseOption('#usergroup-user-search', userGroupInfo.user || 'agent@v2.com')
    UserGroupPage.addUsersBtn.waitForVisible(5000);
    UserGroupPage.addUsersBtn.click();
    browser.element('header*=' + userGroupInfo.user).waitForVisible(5000);
  }

  // Add Checklist to User Group
  if(userGroupInfo.checklist) {
    meweHelper.multiselect.chooseOption('#usergroup-checklist-search', userGroupInfo.checklist)
    UserGroupPage.addChecklistsBtn.waitForVisible(5000);
    UserGroupPage.addChecklistsBtn.click();
    browser.element('header*=' + userGroupInfo.checklist).waitForVisible(5000);
  }

  // Add Property to User Group
  // Create, Read and Update should be default permission for property so that the agent could have impromptu property creation.
  browser.element('permission-level[ng-model="groupPermission.property"] select').selectByVisibleText("Create, Read, and Update");
  if(userGroupInfo.property) {
    meweHelper.multiselect.chooseOption('#usergroup-checklist-search', userGroupInfo.property)
    UserGroupPage.addPropertiesBtn.waitForVisible(5000);
    UserGroupPage.addPropertiesBtn.click();
    browser.element('header*=' + userGroupInfo.property).waitForVisible(5000);
  }
  return assert(UserGroupPage.saveBtn.isVisible());
}
