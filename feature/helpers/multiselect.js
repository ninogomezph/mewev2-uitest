var multiselect = {
  open: (selector) => {
    var multiselect = meweHelper.waitFor(browser.element(selector));
    multiselect.moveToObject();
    multiselect.click();
  },
  /**
  * The id the element you are selecting
  */
  chooseOption: (selector, text,pref='') => {
    if(selector[0] != '#' )
        throw  new Error('multiSelect.chooseOption requires the first args to be an id')

    browser.scroll(selector, 0 , 300);
    browser.element(selector).click();
    browser.pause(500)
    browser.element(selector+"-overlay .search-input").setValue(text)
    browser.pause(5000)
    browser.element(`.multiSelectItem*=${text}`).click();
    browser.pause(5000)
    browser.element(selector).click();
  }
}

module.exports = multiselect;