'use strict';
const LoginPage = require('../pages/login');
const LeftMenu = require('../pages/admin/leftMenu');
const Menu = require('../pages/agent/menu');

module.exports = (role) => {
  if (role === 'ADMIN') {
    LeftMenu.navItemLogout.click();
  } else {
    const menuIsRevealed = browser.element('.show-menu').state === 'success';
    if (!menuIsRevealed) Menu.reveal();
    Menu.navItemLogout.click();
  }
  browser.timeouts('implicit',3000);
  return assert(LoginPage.emailField.isVisible());
};
