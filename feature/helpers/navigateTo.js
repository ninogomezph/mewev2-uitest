    const _ = require('lodash')
    
    var isUnfound = (arg)=> arg.state == 'failure'
    var hasNoChildElements = (ary)=> _.isArray(ary.value) && ary.value.length==0
    
    function recursiveNavigation(arryPath,opts){
      var arg = arryPath.shift()
      var element = arg
      switch(typeof arg){
        case 'string':
          element = browser.element(arg)
          break
        case 'number':
          element = browser.elementIdElement(arg)
          break
        case 'object': 
          if(!arg.selector)
            throw new Error('Object is not a webdriver element')
    
          // this scenario is for when a function caller
          // supplies a webdriver element that does
          // NOT EXIST in the page YET. In this case
          // we attempt to reslect it again, hoping
          // that it would now be visible.
          if( isUnfound(arg) || hasNoChildElements(arg) ) {
            element = browser.element(arg.selector) 
          } 
          break
      }
      if(isUnfound(element))
        throw new Error(`${element.seelector} could not be foudn via navigateTo`)
      element.waitForExist()
      element.click()
      if( arryPath.length == 0)
        return true
      else
        return recursiveNavigation(arryPath,opts)
    }
    
    /**
    * @param {string|element} n1...n2 Accepts infinite arguments. 
    * @example
    *  helper.navigateTo([
    *    "li=Checklist" // accepts selectors
    *    pages.checklistCategories.createCategoryBtn // accepts object
    *  ])
    */
    module.exports = function(arryPath,opts){
      return recursiveNavigation(arryPath,opts)
    }