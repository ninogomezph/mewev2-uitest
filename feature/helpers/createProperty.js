'use strict';
const LeftMenu = require('../pages/admin/leftMenu');
const PropertyCategories = require('../pages/admin/propertyCategories');
const PropertyPage = require('../pages/admin/propertyPage');
const takeScreenshot = require('./takeScreenshot');
const path = require('path');

const defaultProperty = {
  category: 'C5',
  name: 'Carbon Five',
  country: 'US',
  zip: '90401',
  state: 'CA',
  city: 'Santa Monica',
  address: '525 Colorado Ave',
  address2: '',
  longitude: 99.0,
  latitude: 99.1,
};


module.exports = (customPropertyInfo) => {
  let property = Object.assign(defaultProperty, customPropertyInfo);

  if(browser.element("#alert-holder > ul > li").isVisible()) {
    browser.element("#alert-holder > ul > li").click();
  }
  // Navigate to Checklist Category Page
  LeftMenu.navItemProperties.click();
  browser.waitForExist('button=Create Category', 5000);

  // Check if Checklist Category Exist, if not then Create the Checklist Category
  if(!browser.element('=' + property.category).isVisible())
  {
    // Create Checklist Category
    PropertyCategories.createCategoryBtn.click();
    PropertyCategories.newCategoryInput.setValue(property.category);
    PropertyCategories.saveCategoryBtn.waitForExist(5000);
    PropertyCategories.saveCategoryBtn.click();

    browser.pause(1000);
    if(browser.element("#alert-holder > ul > li").isVisible()) {
      browser.element("#alert-holder > ul > li").click();
    }
  }


  // Navigate to list of Property under the Property Category
  browser.waitForExist('=' + property.category, 5000);
  browser.click('=' + property.category);
  browser.waitForExist('a.btn=Create', 5000);
  PropertyPage.createPropertyBtn.click();
  browser.waitForExist('div.btn-action=Save', 5000);

  // Create Property
  PropertyPage.propName.setValue(property.name);
  PropertyPage.propAddress.setValue(property.address);
  PropertyPage.propAddress2.setValue(property.address2);
  PropertyPage.latitude.setValue(property.latitude);
  PropertyPage.longitude.setValue(property.latitude);
  PropertyPage.city.setValue(property.city);
  PropertyPage.state.setValue(property.state);
  PropertyPage.zip.setValue(property.zip);
  PropertyPage.country.setValue(property.country);
  PropertyPage.savePropertyBtn.click();
  browser.pause(1000);

  return assert(browser.element('p=Building has been created').isVisible());
}
