'use strict';
const helpers = require('../helpers');
const config = require('../configurationLoader');
const LeftMenu = require('../pages/admin/leftMenu');
const AgentMenu = require('../pages/agent/menu');

describe('New Property form', function() {
  let newPropertyForm;
  before(() => {
    newPropertyForm = pages.agent.newPropertyForm();
    return helpers.loginAs('AGENT');
  });
  after(() => {
    return meweHelper.logoutAs('AGENT');
  });

  it('Create New Property', function() {
    AgentMenu.reveal();

    browser.pause(500);
    AgentMenu.newProperty.click()

    // Input Property Info
    newPropertyForm.name.waitForVisible(5000);
    newPropertyForm.name.setValue('MeWe Cebu');
    newPropertyForm.address.setValue('N. Escario');
    newPropertyForm.latitude.setValue('10.319481');
    newPropertyForm.longitude.setValue('123.902043');
    newPropertyForm.city.setValue('Cebu City');
    newPropertyForm.state.setValue('Cebu');
    newPropertyForm.zip.setValue('6000');
    newPropertyForm.country.setValue('Philippines');
    newPropertyForm.categoryId.selectByVisibleText('Uncategorized');
    browser.pause(500);

    newPropertyForm.nextButton.click();
    newPropertyForm.confirmAndAddButton.waitForVisible(5000);
    newPropertyForm.confirmAndAddButton.click();

    browser.pause(2000);
    if(browser.alertText()) {
      let alertText = browser.alertText();
      browser.alertAccept();
      return assert(alertText == 'Property has been created.')
    }
    else
    {
      return assert(true);
    }
  });

});
