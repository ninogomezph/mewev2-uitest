const casual = require('casual');
const createUser = require('../admin/usersPage/createUser.spec.js');

describe('AGENT New User change password required', () => {
  const password = 'password';
  const user = {
    username: casual.username,
    email: casual.email,
    password,
    passwordCopy: password,
    firstName: casual.first_name,
    lastName: casual.last_name,
    role: 'AGENT',
  };

  // before does not work well with the reuse pattern
  prepare(function () {
    createUser(user);
  });

  describe('test proper:', function () {
    let changeCredentials;
    before(() => {
      changeCredentials = pages.common.changeCredentials;
      return meweHelper.loginAs(user.username, password, 'AGENT');
    });
    after(() => {
      return meweHelper.logoutAs('AGENT');
    });
    it('modal should display', () => {
      assert(changeCredentials.modal.isVisible());
    });
    it('should be able to input values', () => {
      const values = {
        password,
        newPassword: 'newPassword',
        newPasswordRetype: 'newPassword',
      }
      changeCredentials.setFormValues(values);
    });
    it('submit then modal hides', () => {
      changeCredentials.changePasswordButton.click();
      browser.waitForVisible('#change-credentials-modal', 2000, true);
      changeCredentials.changePasswordAlert.waitForVisible();
      changeCredentials.changePasswordAlert.click();
      browser.pause(500) // an element might still be overlapping with the hamburger menu button
    });
  })
})
