'use strict';

var Page = require('../page.js');
const config = require('../../configurationLoader');
const casual = require('casual');


const CriteriaPage = Object.create(Page, {

  // BUTTONS

  nextButton: Page.makeElementFrom('.i-circle-right.btn-circle'),
  prevButton: Page.makeElementFrom('h3.i-circle-left.btn-circle'),
  fileUploadSelector: Page.makeElementFrom("input[type='file']"),
  uploadedImage: Page.makeElementFrom('#imageTaken'),
  inputTextField: Page.makeElementFrom('input[type=text]'),
  firstAnswerSelector: Page.makeElementFrom('form .row:nth-child(2n) .nradio'),
  textAreaSelector: Page.makeElementFrom('textarea'),
  imagesGallery: Page.makeElementFrom('div.images-gallery'),
  uploadedImages: Page.makeElementsFrom('[ng-repeat="img in images"] div'),
  photoCommentEditor: Page.makeElementsFrom('figure.i-edit'),
  photoCommentsArea: Page.makeElementFrom('section.edit textarea'),
  photoCommentSave: Page.makeElementFrom('figure.i-save'),

  // CRITERIA CONTENTS
  criteriaDescription: Page.makeElementFrom('li.i-info'),
  criteriaPhoto: Page.makeElementFrom('li.i-picture'),
  criteriaImage: Page.makeElementFrom('div.images-bar'),
  criteriaExitDescription: Page.makeElementFrom('h4.i-info'),
  criteriaExitLink: Page.makeElementFrom('h4.i-external-link'),
  criteriaExitPhoto: Page.makeElementFrom('h4.i-picture'),
  criteriaLink: Page.makeElementFrom('li.i-external-link'),
  criteriaLastQuestion: Page.makeElementFrom('li.criteria.ng-scope'),
  linkContent: Page.makeElementFrom('a.mewe-link'),
  descriptionContent: Page.makeElementFrom('p.ng-binding'),
  imageThumbs: Page.makeElementsFrom('div.images-thumbs'),
  photoSwitcher: Page.makeElementFrom('div.images-thumbs.ng-scope'),
  questionHeader: Page.makeElementFrom('div.row.criteria-name'),
  backButton: Page.makeElementFrom('a#back-link'),
  hasAnotherQuestion: {
    value: function() {
      return this.nextButton.isVisible();
    },
  },

  fillInWithRandomAnswers: {
    value: function() {
      var isFileUploadExisting = this.fileUploadSelector.isExisting();
      var isTextFieldVisible = this.inputTextField.isVisible();
      var isTextareaVisible = this.textAreaSelector.isVisible();
      var isFirstAnswerVisible = this.firstAnswerSelector.isVisible();
      var isCriteriaDescriptionVisible = this.criteriaDescription.isVisible();
      var isCriteriaPhotoVisible = this.criteriaPhoto.isVisible();
      var isCriteriaLinkVisible = this.criteriaLink.isVisible();
      var isImageThumbVisible = this.imageThumbs.isVisible();

      var imagePaths = ['./tests/images/penguin.jpg', './tests/images/git.jpg', './tests/images/wat.jpg'];
      var sentence = casual.sentence;
      var paragraph = casual.sentences(5);
      var article = casual.sentences(10);

      // DESCRIPTION

      if (isCriteriaDescriptionVisible) {
        browser.pause(5000);
        this.criteriaDescription.click();
        this.descriptionContent.waitForVisible(5000);
        this.criteriaExitDescription.waitForVisible();
        this.criteriaExitDescription.click();
      }
      // PHOTO EXAMPLE
      if (isCriteriaPhotoVisible) {
        browser.pause(5000);
        this.criteriaPhoto.click();
        if (isImageThumbVisible) {
          this.photoSwitcher.click();
          console.log('photo switcher is clicked!');
        }
        browser.pause(5000);
        this.criteriaImage.waitForVisible(10000);
        this.criteriaExitPhoto.click();
      }

      // LINK EXAMPLE

      if (isCriteriaLinkVisible) {
        browser.pause(5000);
        this.criteriaLink.click();
        this.linkContent.waitForVisible(10000);
        this.linkContent.click();
        browser.pause(500);
        console.log('linked is clicked!');
        // var browserTab = browser.windowHandles().value;
        // browser.switchTab(browserTab + 1);
        browser.pause(500);
        this.criteriaExitLink.click();
      }


      if (isFirstAnswerVisible) {
        this.firstAnswerSelector.click();
      }

      if (isTextFieldVisible) {
        this.inputTextField.setValue(sentence);
      }

      if (isTextareaVisible) {
        this.textAreaSelector.setValue(article);
      }


      for (var i = 0; i < 3; i++) {
        if (isFileUploadExisting && config.UPLOAD_IMAGE) {
          this.fileUploadSelector.scroll();
          this.fileUploadSelector.chooseFile(imagePaths[i]);
          browser.waitUntil(function() {
            var numVisibleImages = this.uploadedImages.value.length;
            return numVisibleImages === i + 1;
          }.bind(this), 25000, 'expected uploaded image to appear');
          browser.waitUntil(function() {
            var numVisiblePhotoEditors = this.photoCommentEditor.value.length;
            var hasNewEditorButton = numVisiblePhotoEditors === i + 1;
            var editorButtonDisplayed =
            browser.elementIdDisplayed(this.photoCommentEditor.value[i].ELEMENT).value;
            return hasNewEditorButton && editorButtonDisplayed;
          }.bind(this), 25000, 'expected new photo editor edit link to show up');

          browser.elementIdClick(this.photoCommentEditor.value[i].ELEMENT);
          this.photoCommentsArea.setValue(paragraph);
          this.photoCommentSave.click();
        }
      }

      this.nextButton.scroll();
      this.nextButton.click();
    },
  },
});


module.exports = CriteriaPage;
