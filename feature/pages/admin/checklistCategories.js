var Page = require('../page.js');

var PropertyCategories = Object.create(Page, {
  createCategoryBtn: Page.makeElementFrom('button=Create Category'),
  saveCategoryBtn: Page.makeElementFrom('div.edit div[title="done"]'),
  searchInput: Page.makeElementFrom('input[placeholder="Search all checklists"]'),
  newCategoryInput: Page.makeElementFrom('div.edit input[placeholder="new category"]'),
  createForm: Page.makeElementFrom('.mewe-list.create-form'),
  createdCategory: Page.makeElementFrom('li.category.created'),
  createAlert: Page.makeElementFrom('p=Category created!'),
  createChecklistBtn: Page.makeElementFrom('span=Create Checklist'),
});

module.exports = PropertyCategories;
