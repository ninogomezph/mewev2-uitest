var Page = require('../page.js');

var Property = Object.create(Page, {
  createPropertyBtn: Page.makeElementFrom("span=Create"),
});

module.exports = Property;
