var Page = require('../page.js');
var EmailPage = Object.create(Page, {


  emailHeader: Page.makeElementFrom('header=Email Notification'),
  emailPanel: Page.makeElementFrom('.ta-scroll-window >.ta-bind'),
  addNotification: Page.makeElementFrom('#sub-body > section > div > div:nth-child(5) > div > section > div:nth-child(1) > div.column.medium-8 > div'),


  emailTo: Page.makeElementFrom('#sub-body > section > div > div:nth-child(5) > div > section > div:nth-child(2) > div > div > div > div.column.large-9 > section:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div.column.small-10 > input'),
  emailAdd: Page.makeElementFrom('#sub-body > section > div > div:nth-child(5) > div > section > div:nth-child(2) > div > div > div > div.column.large-9 > section:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > div'),
  emailCcAdd: Page.makeElementFrom('#sub-body > section > div > div:nth-child(5) > div > section > div:nth-child(2) > div > div > div > div.column.large-9 > section:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div'),
  emailCc: Page.makeElementFrom('#sub-body > section > div > div:nth-child(5) > div > section > div:nth-child(2) > div > div > div > div.column.large-9 > section:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div.column.small-10 > input'),

  emailSubject: Page.makeElementFrom('input[placeholder="Subject"]'),
  requirementsBtn: Page.makeElementFrom('.btn-action=Add Requirement Set'),
  reqEditor: Page.makeElementFrom('#sub-body > section > div > div:nth-child(5) > div > section > div:nth-child(2) > div > div > div > div.column.large-9 > section:nth-child(2) > div.column.large-12.ng-scope > div > div:nth-child(2) > div > div > div:nth-child(1) > div.column.large-6'),
  reqItem: Page.makeElementFrom('#sub-body > section > div > div:nth-child(5) > div > section > div:nth-child(2) > div > div > div > div.column.large-9 > section:nth-child(2) > div.column.large-12.ng-scope > div > div:nth-child(2) > div > div > div:nth-child(1) > div.column.large-6 > select > option:nth-child(even)'),
  addQuestion: Page.makeElementFrom('div=Add Question'),
  questionPlaceholder: Page.makeElementFrom('div.column.large-6:select'),
  questionHeader: Page.makeElementFrom('header=Question Editor'),

  reportHeader: Page.makeElementFrom('#sub-body > section > div > div:nth-child(6) > div > header'),
  requireAll: Page.makeElementFrom('input[type="checkbox"]'),

  answerDropdown: Page.makeElementFrom('#id > header > section'),

  menuAll: Page.makeElementFrom('#id > section > menu > div:nth-child(1)'),
  // menuNone: Page.makeElementFrom('section > menu> div=NONE'),

  btnSave: Page.makeElementFrom('.btn-action.fix'),

  emailItem: Page.makeElementFrom('.cg'),
  emailDelete:Page.makeElementFrom('#sub-body > section > div > div:nth-child(5) > div > section > div:nth-child(2) > div > div > div > div.column.large-3 > div.icon-btn.i-delete'),
  emailPopDelete: Page.makeElementFrom('li.slide-down'),

  scrollToEmail:{
    value:function() {
        var isQuestionHeaderVisible = this.questionHeader.isVisible();
        if(isQuestionHeaderVisible){
          this.addNotification.scroll(0, browser.height);
          this.addNotification.click();
        }
      }
    },

  inputEmailValues:{
    value:function() {
      var isToEmailVisible = this.emailTo.isVisible();
      var isCcEmailVisible = this.emailAdd.isVisible();
      var isSubjectVisible = this.emailSubject.isVisible();

      if((isToEmailVisible) ){
        this.emailTo.setValue('gabrielle@mewe.org');
        this.emailAdd.click();
      }

      if(isCcEmailVisible){
        this.emailCc.setValue('gabrielledurano95@gmail.com');
        this.emailCcAdd.click();
      }

      if(isSubjectVisible){
        this.emailSubject.setValue('Email From Dota 2 checklist');
      }
    }
  },

  addRequirement:{
    value:function() {
      var isReqBtnVisible = this.requirementsBtn.isVisible();
      var isEmailPanelVisible = this.emailPanel.isVisible();
      var isBtnSaveVisible = this.btnSave.isVisible();

      if(isReqBtnVisible){
        this.requirementsBtn.click();
        browser.pause(500);
        this.reqEditor.click();
        this.reqItem.click();
        this.addQuestion.click();
        this.answerDropdown.click();
        this.menuAll.click();
        browser.pause(5000);
      }


      if(isEmailPanelVisible){
        this.emailPanel.click();
        this.emailPanel.setValue('Hey Admin, somebody is answering the first question on checklist DOTA-2 copy.');
    }

       if(isBtnSaveVisible){
          this.btnSave.click();
        }
  }
}, // end of add requirement

});

module.exports = EmailPage;
