var Page = require('../page.js');
const casual = require('casual');

var User = Object.create(Page, {
  firstName: Page.makeElementFrom('input[ng-model="user.profile.firstName"]'),
  lastName: Page.makeElementFrom('input[ng-model="user.profile.lastName"]'),
  username: Page.makeElementFrom('input[ng-model="user.username"]'),
  password: Page.makeElementFrom('input[ng-model="user.password"]'),
  passwordCopy: Page.makeElementFrom('input[ng-model="user.passwordCopy"]'),
  email: Page.makeElementFrom('input[ng-model="user.email"]'),
  adminRole: Page.makeElementFrom('[value="ADMIN"]'),
  subAdminRole: Page.makeElementFrom('[value="SUB_ADMIN"]'),
  agentRole: Page.makeElementFrom('[value="AGENT"]'),
  saveButton: Page.makeElementFrom('div[ng-click="save()"]'),
  profileAlert: Page.makeElementFrom('p=Please provide profile details First Name and Last Name'),
  usernameAlert: Page.makeElementFrom('p=Please provide a unique username'),
  passwordAlert: Page.makeElementFrom('p=Please provide a login password'),
  passwordCopyAlert: Page.makeElementFrom('p=Please retype your password copy correctly'),
  emailValidAlert: Page.makeElementFrom('p=Please provide a valid email'),
  emailAlert: Page.makeElementFrom('p=Please provide a unique email'),
  createdAlert: Page.makeElementFrom('p=User has been successfully created'),
  selectRole:{
    value:function (role) {
      if (role === 'ADMIN') this.adminRole.click();
      if (role === 'SUB_ADMIN') this.subAdminRole.click();
      if (role === 'AGENT') this.agentRole.click();
    }
  },
});

module.exports = User;
