var Page = require('../page.js');

var ChecklistPage= Object.create(Page,{
  name: Page.makeElementFrom('input[ng-model="checklist.name"]'),
  description: Page.makeElementFrom('textarea[ng-model="checklist.description"]'),
  saveBtn: Page.makeElementFrom('div=Save'),
  createAlert: Page.makeElementFrom('p=Checklist has been created'),
});

module.exports = ChecklistPage;
