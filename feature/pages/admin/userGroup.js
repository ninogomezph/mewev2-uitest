var Page = require('../page.js');

var UserGroup = Object.create(Page, {
  createUserGroupBtn: Page.makeElementFrom("span=Create User Group"),
  saveBtn: Page.makeElementFrom('div=Save'),
  cancelBtn: Page.makeElementFrom('div=Cancel'),
  deleteBtn: Page.makeElementFrom('div=Delete'),
  addUsersBtn: Page.makeElementFrom('div=Add Selected Users'),
  addChecklistsBtn: Page.makeElementFrom('div=Add Selected Checklists'),
  addPropertiesBtn: Page.makeElementFrom('div=Add Selected Properties'),
  inputUserGroupName: Page.makeElementFrom('input[ng-model="group.name"]'),
  inputDescription: Page.makeElementFrom('input[ng-model="group.description"]'),
  saveAlert: Page.makeElementFrom('p=User Group saved.'),
  inputSearchMembersToAdd: Page.makeElementFrom("input[placeholder='Search Users']"),
});

module.exports = UserGroup;
