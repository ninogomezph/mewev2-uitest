var Page = require('../page.js');

var SettingsPage = Object.create(Page, {
  txtSettings: Page.makeElementFrom('div=Settings'),
  logosTab: Page.makeElementFrom('div=Logos'),
  AuthTab: Page.makeElementFrom('div=Auth'),
  MobileTab: Page.makeElementFrom('div=Mobile'),
  GeneralTab: Page.makeElementFrom('div=General'),
  loginScreenOption: Page.makeElementFrom('div=Login Screen'),
  navBarOption: Page.makeElementFrom('div=Navigation Bar'),
  btnReplace: Page.makeElementFrom('div[ng-click="replace()"]'),
  btnFile: Page.makeElementFrom("input[type='file']"),
  progressLoad: Page.makeElementFrom('div.progress.round'),
  uploadedLogo: Page.makeElementFrom('img'),
  headingUpload: Page.makeElementFrom('h3=Upload Image'),
  value: function() {
    'use strict';
    var images = ['./tests/logos/lupa.jpeg', './tests/logos/nchh.jpeg', './tests/logos/tarry.jpeg'];
    var randImages = images[Math.floor(Math.random() * images.length)];
    var isBtnReplaceVisible = this.btnReplace.isExisting();
    var isBtnFileVisible = this.btnFile.isExisting();
    console.log('uploading here');
    if (isBtnFileVisible || isBtnReplaceVisible) {
      this.btnFile.chooseFile(randImages);
      this.uploadedLogo.waitForVisible();
      console.log('chose photo!');
    }
  },
  // Feature: allow admin to login as agent
  toggleAllowLoginAsAgentOff: Page.makeElementFrom('label[for="admin:coinspect-login_off"]'),
  toggleAllowLoginAsAgentOn: Page.makeElementFrom('label[for="admin:coinspect-login_on"]'),
  //emails tab

   emailsTab:Page.makeElementFrom('div=Emails'),
   createEmail:Page.makeElementFrom
   ('#sub-body > section > div > div > div:nth-child(1) > section:nth-child(3) > div > div > header > div > div > a'),
   emailPanel: Page.makeElementFrom('section.mewe-panel'),
   emailTo: Page.makeElementFrom('input[name=to]'),
   emailToTxt: Page.makeElementFrom('label=To'),
   emailAdd: Page.makeElementFrom('div.i-add'),
   emailCcTxt:Page.makeElementFrom('label=Cc'),
   emailCc:Page.makeElementFrom('#sub-body > section > div > div > div:nth-child(2) > div > section > div > form > div > div:nth-child(2) > div:nth-child(2) > div.column.small-10 > input'),
   emailccAdd:Page.makeElementFrom('#sub-body > section > div > div > div:nth-child(2) > div > section > div > form > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div'),
   emailSubject:Page.makeElementFrom('input[name=subject]'),
   emailNotif:Page.makeElementFrom('.mewe-panel select'),
   inspectionStarted:Page.makeElementFrom('option=Inspection Started'),
   inspectionDone:Page.makeElementFrom('option=Inspection Done'),
   inspectionFinal:Page.makeElementFrom('option=Inspection Report Finalized'),
   emailContent:Page.makeElementFrom('.ta-bind'),
   emailSave:Page.makeElementFrom('button[type="Submit"]'),

   // Mobile Tab
   toggleReviewHistoryOff: Page.makeElementFrom('label[for="coinspect:review-history_off"]'),
   toggleReviewHistoryOn: Page.makeElementFrom('label[for="coinspect:review-history_on"]'),

  });
module.exports = SettingsPage;
