var Page = require('../page.js');

var PropertyCategories = Object.create(Page, {
  createCategoryBtn: Page.makeElementFrom("button.property__action-btn=Create Category"),
  saveCategoryBtn: Page.makeElementFrom('div.edit div[title="done"]'),

  searchBarInput: Page.makeElementFrom('input[placeholder="Search all properties"]'),
  newCategoryInput: Page.makeElementFrom('div.edit input[placeholder="new category"]'),

  createdCategory: Page.makeElementFrom("a=Selenium Sample Category"),
  createdProperty: Page.makeElementFrom('a*=Sample Property Name'),

  createForm: Page.makeElementFrom('.mewe-list.create-form'),
  createdCategory: Page.makeElementFrom('li.category.created'),

  categories : Page.makeElementsFrom('li.category h3'),
});
module.exports = PropertyCategories;
