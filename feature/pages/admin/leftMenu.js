var Page = require('../page.js');

var LeftMenu = Object.create(Page, {
  navItemProperties: Page.makeElementFrom('li=Properties'),
  navItemAssignments: Page.makeElementFrom('li=Assignments'),
  navItemLogout: Page.makeElementFrom('li=Logout'),
  navItemSettings: Page.makeElementFrom('li=Settings'),
  checklists: Page.makeElementFrom('li=Checklists'),
  navItemUsers: Page.makeElementFrom('li=Users'),
  navItemSwitchView: Page.makeElementFrom('li=Agent Mode'),
  userGroups: Page.makeElementFrom('li=User Groups')
});

module.exports = LeftMenu;
