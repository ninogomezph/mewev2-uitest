var Page = require('../page.js');

var DashboardPage = Object.create(Page, {
  inspectionGraphTab: Page.makeElementFrom('li=Inspection Graph'),
  inspectionGraph: Page.makeElementFrom('.line-graph svg'),
  inspectionTableTab: Page.makeElementFrom('li=Inspection Table'),
  inspectionTable: Page.makeElementFrom('.lm_item_container table'),
});

module.exports = DashboardPage;
