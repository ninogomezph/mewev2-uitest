var Page = require('../page.js');

module.exports =  Object.create(Page, {
  holder: Page.makeElementFrom("#alert-holder"),
  alerts: Page.makeElementsFrom("#alert-holder li"),
});
