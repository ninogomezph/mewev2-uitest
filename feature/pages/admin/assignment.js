var Page = require('../page.js');

var AssignmentPage = Object.create(Page, {
  btnCreateAssignments: Page.makeElementFrom('.btn'),
  txtInspectionTab: Page.makeElementFrom('div=Inspections'),
  btnDeleteInspection: Page.makeElementFrom('.assignment-element:first-child .i-trash'),
  listAssignment: Page.makeElementFrom('ul.mewe-list-assignments'),
  listHeader: Page.makeElementFrom('div.assignment-element header'),
  firstTimeStamp: Page.makeElementFrom('li.assignment-element:first-child .date'),
  firstTimeStampText:
  Page.getInteractableElementText('li.assignment-element:first-child .date'),
  assignmentItem: Page.makeElementFrom('li.assignment-element'),
  deleteFirstInspection: {
    value: function() {
      this.btnDeleteInspection.waitForVisible(10000);
      this.btnDeleteInspection.click();
    },
  },
  btnYes: Page.makeElementFrom('div=Yes'),
});


module.exports = AssignmentPage;
