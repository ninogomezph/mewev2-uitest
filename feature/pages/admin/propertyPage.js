var Page = require('../page.js');

var Property = Object.create(Page, {
  savePropertyBtn: Page.makeElementFrom('[ng-click="save(building)"]'),
  cancelPropertyBtn: Page.makeElementFrom('div=Cancel'),

  buildingId: Page.makeElementFrom('input[placeholder="Building Id"]'),
  importSrc: Page.makeElementFrom('input[placeholder="Import Source"]'),
  propName: Page.makeElementFrom('input[placeholder="Property Name"]'),
  propAddress: Page.makeElementFrom('input[placeholder="Address"]'),
  propAddress2: Page.makeElementFrom('input[placeholder="Address Line 2"]'),
  latitude: Page.makeElementFrom('input[ng-model="building.latitude"]'),
  longitude: Page.makeElementFrom('input[ng-model="building.longitude"]'),
  zip: Page.makeElementFrom('input[ng-model="building.zip"]'),
  city: Page.makeElementFrom('input[ng-model="building.city"]'),
  state: Page.makeElementFrom('input[ng-model="building.state"]'),
  country: Page.makeElementFrom('input[ng-model="building.country"]'),
  createAlert: Page.makeElementFrom('p=Building has been created'),
  addressSearch: Page.makeElementFrom('#address-search button.toggle-button'),
  addressSearchInput: Page.makeElementFrom('.search-input'),
  addressSearchResults: Page.makeElementsFrom('.multiSelectItem'),
});

module.exports = Property;
