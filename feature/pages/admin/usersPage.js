var Page = require('../page.js');

var Users = Object.create(Page, {
  createButton: Page.makeElementFrom('a=Create New User'),
});

module.exports = Users;
