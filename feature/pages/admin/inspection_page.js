var Page = require('../page.js');

var InspectionPage = Object.create(Page, {
  firstGroup: Page.makeElementFrom('ul.criteria-groups > li:first-child'),
  firstCriteria: Page.makeElementFrom('ul.criteria-groups > li:first-child ul li:first-child'),
  btnMarkAsDone: Page.makeElementFrom('div=MARK AS DONE'),
  btnCreateReport: Page.makeElementFrom('div=CREATE REPORT'),
  checklistDonePopup: Page.makeElementFrom('div#done-popup'),
  btnDownloadReport: Page.makeElementFrom('div=DOWNLOAD REPORT'),


});


module.exports = InspectionPage;
