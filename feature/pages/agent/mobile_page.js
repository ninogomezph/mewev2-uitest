var Page = require('../page.js');
var MobilePage = Object.create(Page, {
  mobileTab: Page.makeElementFrom('div=Mobile'),
  historyTitle: Page.makeElementFrom('div=Building History'),
  historyLimitation: Page.makeElementFrom('div=Building History Limitation'),

  // Building History
  historyButtonOn: Page.makeElementFrom('label[for="building_history_on"]'),
  historyButtonOff: Page.makeElementFrom('label[for="building_history_off"]'),

  // Building History Limitation

  buildingLimitOn: Page.makeElementFrom('label[for="building_history_limit_on"]'), // same user
  buildingLimitOff: Page.makeElementFrom('label[for="building_history_limit_off"]'), // all user
  listOfToggles: Page.makeElementFrom('div.row'),
  buildingLimitValue: Page.makeElementFrom('input[name="building_history_limit"]:checked'), // get the value
  buildingSwitchSame: {
    value : function() {
      var isBuildingLimitOn = this.buildingLimitOn.isVisible();
      if(isBuildingLimitOn){
        this.buildingLimitOn.click();
      }
    }
  },
  buildingSwitchAll: {
      value : function() {
        var isBuildingLimitOff = this.buildingLimitOff.isVisible();
        if(isBuildingLimitOff){
          this.buildingLimitOff.click();
        }
      }
    },  

  // Inspection Search 

  inspectionLimit: Page.makeElementFrom('div=Inspection Search Limitation'),
  inspectionLimitOn: Page.makeElementFrom('label[for="inspection_search_limit_on"]'), // Same user
  inspectionLimitOff: Page.makeElementFrom('label[for="inspection_search_limit_off"]'), // All user
  inspectionLimitValue: Page.makeElementFrom('input[name="inspection_search_limit"]:checked'), // get the value
  inspectionSwitchLimitSame:{
    value: function() {
          var isInspectionLimitOn = this.inspectionLimitOn.isVisible();
          if(isInspectionLimitOn){
            this.inspectionLimitOn.click();
          }
    }
  },
  inspectionSwitchLimitAll:{
    value: function() {
          var isInspectionLimitOff = this.inspectionLimitOff.isVisible();
          if(isInspectionLimitOff){
            this.inspectionLimitOff.click();
          }
    },
  },
});

module.exports = MobilePage;
