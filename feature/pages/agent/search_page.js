var Page = require('../page.js');

var Search = Object.create(Page, {
  searchTxt: Page.makeElementFrom('header=Search'),
  searchField: Page.makeElementFrom('input[name="keyword"]'),
  btnContinue: Page.makeElementFrom('button=Continue'),
  btnBack: Page.makeElementFrom('button=Back'),
  searchItem: Page.makeElementFrom('li.ng-scope.li-icon-list'),
  btnCreatePDF: Page.makeElementFrom('button=Create PDF'),
  btnDownloadPDF: Page.makeElementFrom('div.btn-action.radius'),

});

module.exports = Search;
