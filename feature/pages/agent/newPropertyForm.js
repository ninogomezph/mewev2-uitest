module.exports = function () {
  const Page = pages.page;

  const elements = {
    getMyLocationButton: Page.makeElementFrom('button=Get my location'),
    enterManuallyButton: Page.makeElementFrom('button=Enter manually'),
    confirmAndAddButton: Page.makeElementFrom('button=Confirm and Add'),
    nextButton: Page.makeElementFrom('button=Next'),
  }

  const inputFields = [
    'name',
    'address',
    'latitude',
    'longitude',
    'city',
    'state',
    'zip',
    'country',
    'categoryId',
  ]

  inputFields.forEach((field) => {
    elements[field] = Page.makeElementFrom(`[ng-model="bldg.${field}"]`);
  });

  return Object.create(Page, elements);
};
