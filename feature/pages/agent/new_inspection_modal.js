var Page = require('../page.js');

var NewInspectionModal = Object.create(Page, {
  inspectionDropdown: Page.makeElementFrom('span.selection'),
  inspectionDropdownHighlightedField: Page.makeElementFrom('.select2-results__option--highlighted'),
  inspectionDropdownValue: Page.makeElementFrom('.select2-hidden-accessible'),
  propertySearchField: Page.makeElementFrom("input[type='search']"),
  checklistMenu: Page.makeElementFrom(
    '#create-inspection > form > div:nth-child(4) > div > select'
  ),
  createInspection: Page.makeElementFrom(
    '#create-inspection > form > div:nth-child(5) > div > button'
  ),

  openPropertyMenu: {
    value: function() {
      this.inspectionDropdownValue.waitForVisible();
      browser.pause(1000);
      this.inspectionDropdown.click();
      this.propertySearchField.waitForVisible();
    },
  },
});

module.exports = NewInspectionModal;
