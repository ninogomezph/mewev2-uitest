var Page = require('../page.js');

var Menu = Object.create(Page, {
  body: Page.makeElementFrom('body'),
  hamburger: Page.makeElementFrom('.hamburger-button'),
  navItemNew: Page.makeElementFrom('a=New Inspection'),
  newProperty: Page.makeElementFrom('a=New Property'),
  navItemLogout: Page.makeElementFrom('a=Logout'),
  navItemSearch: Page.makeElementFrom('a=Search'),
  navItemReviewHistory: Page.makeElementFrom('a=Review'),
  navItemSwitchView: Page.makeElementFrom('a=Admin Mode'),
  navHome: Page.makeElementFrom('div.i-circle-left '),
  reveal: {
    value: function() {
      this.hamburger.click();
      this.navItemNew.waitForVisible();
      this.navItemSearch.waitForVisible();
      this.navItemLogout.waitForVisible();
    },
  },
});

module.exports = Menu;
