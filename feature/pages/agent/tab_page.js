var Page = require('../page.js');

var Tab = Object.create(Page, {
  newTab: Page.makeElementFrom('h6=NEW'),
  openTab: Page.makeElementFrom('h6=OPEN'),
  doneTab: Page.makeElementFrom('h6=DONE'),
  propertyName: Page.makeElementFrom('div.i-down'),
  loadHistory: Page.makeElementFrom('a.get-history'),
  hideHistory: Page.makeElementFrom('a.hide-history'),
  reviewButton: Page.makeElementFrom('div=REVIEW'),
  coinspectTitle: Page.makeElementFrom('a=CoInspect'),
  listHistory: Page.makeElementFrom('ul.chks'),
});


module.exports = Tab;
