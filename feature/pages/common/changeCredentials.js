var Page = require('../page.js');

var Menu = Object.create(Page, {
  modal: Page.makeElementFrom('#change-credentials-modal'),
  changePasswordForm: Page.makeElementFrom('[name="cpForm"]'),
  currentPassword: Page.makeElementFrom('[ng-model="changePasswordForm.password"]'),
  newPassword: Page.makeElementFrom('[name="newPassword"]'),
  newPasswordRetype: Page.makeElementFrom('[ng-model="changePasswordForm.retype"]'),
  changePasswordButton: Page.makeElementFrom('[ng-click="changePassword()"]'),
  changePasswordAlert: Page.makeElementFrom('p=successfully updated password!'),
  setFormValues: {
    value: function (values) {
      if (values.password) this.currentPassword.setValue(values.password);
      if (values.newPassword) this.newPassword.setValue(values.newPassword);
      if (values.newPasswordRetype) this.newPasswordRetype.setValue(values.newPassword);
    },
  }
});

module.exports = Menu;
