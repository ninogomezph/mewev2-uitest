'use strict';

describe('Admin Switch View', function() {
  let LeftMenu;
  let AgentMenu;
  before(() => {
    LeftMenu = pages.admin.leftMenu;
    AgentMenu = pages.agent.menu;
    return meweHelper.loginAs('ADMIN');
  });
  after(() => {
    return meweHelper.logoutAs('ADMIN');
  });
  it('switch to AGENT from ADMIN!', () => {
    LeftMenu.navItemSwitchView.click();
    browser.timeoutsImplicitWait(3);
    return assert.equal(browser.getTitle(), 'MeWe Agent');
  });
  it('switch to ADMIN from AGENT!', () => {
    AgentMenu.reveal();
    AgentMenu.navItemSwitchView.click();
    browser.timeoutsImplicitWait(3);
    return assert.equal(browser.getTitle(), 'MeWe');
  });
});
