const pageElements = pages.admin;
const propertyPage = pageElements.propertyPage;

const casual = require('casual');
const _ = require('lodash');
const reusedByAnotherSpec = !(module.parent.id.split('/').pop() === 'mocha.js');

/*
propertyConfiguration
  name: ''
  address:
  ...
*/
function run(propertyConfiguration) {
  if (reusedByAnotherSpec && !propertyConfiguration) throw new Error('please define property configuration');

  const property = _.defaults(propertyConfiguration, {
    name: casual.name,
    address: casual.address1,
    address2: casual.address2,
    state: casual.state,
    city: casual.city,
    zip: '210', // makes tz suggestion predictable: EST
    longitude: casual.longitude,
    latitude: casual.latitude,
    country: casual.country,
  });

  before(() => {
    meweHelper.loginAs('ADMIN');
    meweHelper.navigateTo([
      pageElements.leftMenu.navItemProperties,
      pageElements.propertyCategories.categories,
      pageElements.propertiesPage.createPropertyBtn,
    ]);
    return;
  });
  after(() => meweHelper.logoutAs('ADMIN'));

  it('can input values and save', function () {
    propertyPage.savePropertyBtn.waitForVisible();

    propertyPage.propName.setValue(property.name);
    propertyPage.propAddress.setValue(property.address);
    propertyPage.propAddress2.setValue(property.address2);
    propertyPage.latitude.setValue(property.latitude);
    propertyPage.longitude.setValue(property.longitude);
    propertyPage.city.setValue(property.city);
    propertyPage.state.setValue(property.state);
    propertyPage.zip.setValue(property.zip);
    propertyPage.country.setValue(property.country);

    propertyPage.savePropertyBtn.click();
    propertyPage.createAlert.waitForVisible();
  });
  it('changes persist after refresh', function () {
    browser.refresh();
    browser.pause(500);
    expect(propertyPage.propName.getValue()).to.be.equal(property.name);
    expect(propertyPage.propAddress.getValue()).to.be.equal(property.address);
    expect(propertyPage.zip.getValue()).to.be.equal(property.zip);
    expect(propertyPage.city.getValue()).to.be.equal(property.city);
    expect(propertyPage.state.getValue()).to.be.equal(property.state);
    expect(propertyPage.country.getValue()).to.be.equal(property.country);
    expect(+propertyPage.latitude.getValue()).to.be.equal(+property.latitude);
    expect(+propertyPage.longitude.getValue()).to.be.equal(+property.longitude);
  });

  it('should display the building id', function () {
    browser.refresh();
    browser.pause(500);

    const url = browser.getUrl();
    const buildingId = url.match(new RegExp(/\d+\/(\d+)/))[1];

    expect(propertyPage.buildingId.getValue()).to.be.equal(buildingId);
  });

  it('should display the import source', function () {
    browser.refresh();
    browser.pause(500);

    expect(propertyPage.importSrc.getValue()).to.contain('imported by');
  });
}

describe('should be able to create a property', () => {
  if (!reusedByAnotherSpec) run();
});

module.exports = run;
