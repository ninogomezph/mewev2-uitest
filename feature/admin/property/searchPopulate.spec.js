
const pageElements = pages.admin;
const propertyPage = pageElements.propertyPage;

describe('Property Page', function () {
  before(() => {
    meweHelper.loginAs('ADMIN');
    meweHelper.navigateTo([
      pageElements.leftMenu.navItemProperties,
      pageElements.propertyCategories.categories,
      pageElements.propertiesPage.createPropertyBtn,
    ])
    return;
  });
  after(() => meweHelper.logoutAs('ADMIN'));

  describe('search, populate and create', function () {
    it('can search a properties', function () {
      meweHelper.waitFor(propertyPage.addressSearch).click();
      browser.pause(1000);
      propertyPage.addressSearchInput.setValue('Empire State Building');
      browser.pause(5000);
      expect(propertyPage.addressSearchResults.value).to.to.have.length.above(3, 'Results should populate');
    });

    it('can populate the fields', function () {
      browser.pause(10000);
      browser.elementIdClick(propertyPage.addressSearchResults.value[1].ELEMENT);
      browser.pause(1000);
      expect(propertyPage.propAddress.getValue()).to.be.equal('5th Avenue');
      expect(propertyPage.zip.getValue()).to.be.equal('10018');
      expect(propertyPage.city.getValue()).to.be.equal('New York');
      expect(propertyPage.state.getValue()).to.be.equal('New York');
      expect(propertyPage.country.getValue()).to.be.equal('United States');
      expect(propertyPage.latitude.getValue()).to.be.equal('40.748517');
      expect(propertyPage.longitude.getValue()).to.be.equal('-73.986027');
    });
  });
});
