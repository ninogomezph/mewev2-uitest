const _ = require('lodash');
const createChecklist = require('./createChecklist.spec.js');

const checklistArray = [
  {
    name: 'Date',
  },
  {
    name: 'Test',
  },
  {
    name: 'Book',
  },
];
const LeftMenu = pages.admin.leftMenu;
const ChecklistCategoriesPage = pages.admin.checklistCategories;

describe('Checklist Category Page Search bar', () => {
  checklistArray.forEach((checklist) => {
    prepare(`Creating Checklings ${checklist.name}`, () => {
      createChecklist(checklist);
    });
  });

  describe('test proper: ', () => {
    before(() => {
      meweHelper.loginAs('ADMIN');
      LeftMenu.checklists.click();
      ChecklistCategoriesPage.searchInput.waitForVisible();
      return;
    });
    after(() => meweHelper.logoutAs('ADMIN'));

    it('should be able to search checklist name Book', () => {
      ChecklistCategoriesPage.searchInput.setValue('book');
      browser.element('h5=Book').waitForVisible(6000);
    });


    it('should be able to search "te" then the results will be "Date" and "Test"', () => {
      ChecklistCategoriesPage.searchInput.setValue('te');
      browser.element('h5=Test').waitForVisible(6000);
      browser.element('h5=Date').waitForVisible(6000);
    });
  });
});
