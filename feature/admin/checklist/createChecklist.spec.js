const casual = require('casual');
const _ = require('lodash');
const reusedByAnotherSpec = !(module.parent.id.split('/').pop() === 'mocha.js');

/*
checklistConfiguration
  name: ''
  description: ''
*/
function run(checklistConfiguration) {
  if (reusedByAnotherSpec && !checklistConfiguration) throw new Error('please define checklist configuration');
  let LeftMenu;
  let ChecklistPage;
  let ChecklistCategoriesPage;

  const checklist = _.defaults(checklistConfiguration, {
    name: casual.title,
    description: casual.description,
  });

  before(() => {
    LeftMenu = pages.admin.leftMenu;
    ChecklistPage = pages.admin.checklist;
    ChecklistCategoriesPage = pages.admin.checklistCategories;
    return meweHelper.loginAs('ADMIN');
  });
  after(() => {
    return meweHelper.logoutAs('ADMIN');
  });
  it('should be able to go to Checklist Categories page and create a new category', () => {
    LeftMenu.checklists.click();
    ChecklistCategoriesPage.createCategoryBtn.waitForVisible();
    ChecklistCategoriesPage.createCategoryBtn.click();
    ChecklistCategoriesPage.newCategoryInput.setValue(checklist.name);
    ChecklistCategoriesPage.saveCategoryBtn.click();
    ChecklistCategoriesPage.createAlert.waitForVisible();
  });
  it('should be able to go to a category, then checklist editor', () => {
    ChecklistCategoriesPage.createdCategory.click();
    ChecklistCategoriesPage.createChecklistBtn.waitForVisible();
    ChecklistCategoriesPage.createChecklistBtn.click();
    ChecklistPage.saveBtn.waitForVisible();
  });
  it('should be able to input name and description and save', () => {
    ChecklistPage.name.setValue(checklist.name);
    ChecklistPage.description.setValue(checklist.description);
    ChecklistPage.saveBtn.click();
    ChecklistPage.createAlert.waitForVisible();
  });
}

describe('should be able to create a checklist', () => {
  if (!reusedByAnotherSpec) run();
});

module.exports = run;
