var config = require('../configurationLoader');
var LeftMenu = require('../pages/admin/leftMenu');
var LoginPage = require('../pages/login');
var PropertyCategoriesPage = require('../pages/admin/propertyCategories.js');

describe('Admin building catagories properties search', function() {

  before(() => {
    return meweHelper.loginAs('ADMIN');
  });
  after(() => {
    return meweHelper.logoutAs('ADMIN');
  });

  it('goes to properties page', function() {
    LeftMenu.navItemProperties.click();
    PropertyCategoriesPage.createCategoryBtn.waitForVisible();
    PropertyCategoriesPage.searchBarInput.waitForVisible();
    assert(PropertyCategoriesPage.createCategoryBtn.isVisible());
  });

  it('Create a new category then it will appear in the list', function() {
    PropertyCategoriesPage.createdCategory.waitForVisible();

    var count = browser.elements('li.category.created').value.length;

    browser.pause(10000);
    var newCategoryName = 'Selenium Sample Property Category';
    PropertyCategoriesPage.createCategoryBtn.click();
    PropertyCategoriesPage.createForm.waitForVisible();
    PropertyCategoriesPage.newCategoryInput.setValue(newCategoryName);
    PropertyCategoriesPage.saveCategoryBtn.click();

    browser.pause(1000);
    var newCount = browser.elements('li.category.created').value.length;

    expect(newCount).is.equal(count + 1);
    assert(!PropertyCategoriesPage.createForm.isVisible(), '.mewe-list.create-form should be hidden');
  });
});
