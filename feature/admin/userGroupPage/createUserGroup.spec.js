const casual = require('casual');
const _ = require('lodash');
const reusedByAnotherSpec = !(module.parent.id.split('/').pop() === 'mocha.js');

const createUser =  require('../usersPage/createUser.spec.js');
const createChecklist = require('../checklist/createChecklist.spec.js');
const createProperty = require('../property/createProperty.spec.js');
/*
userGroupConfiguration
  users: [string]
  properties: [string]
  checklists: [string]
*/

function run(userGroupConfiguration) {
  if (reusedByAnotherSpec && !userGroupConfiguration) throw new Error('please define usergroup configuration');
  let LeftMenu;
  let UserGroupPage;

  const group = {
    name: (reusedByAnotherSpec ? userGroupConfiguration.name : casual.title),
    description: (reusedByAnotherSpec ? userGroupConfiguration.description : casual.short_description),
    users: (reusedByAnotherSpec ? (userGroupConfiguration.users || []) : [ { email: 'agent@v2.com' }]),
    checklists: (reusedByAnotherSpec ? (userGroupConfiguration.checklists || []) : [ {name: 'Checklist 1' }]),
    properties: (reusedByAnotherSpec ? (userGroupConfiguration.users || []) : [ { name: 'Property 1' }]),
  }

  group.users.forEach((user) => {
    if (user.email !== 'agent@v2.com') {
      prepare(`create user: ${user.email}`, () => {
        createUser(user);
      });
    }
  });

  group.checklists.forEach((checklist) => {
    prepare(`create checklist: ${checklist.name}`, () => {
      createChecklist(checklist);
    });
  });

  group.properties.forEach((property) => {
    prepare(`create property: ${property.name}`, () => {
      createProperty(property);
    });
  });

  describe('test proper:', () => {
    before(() => {
      LeftMenu = pages.admin.leftMenu;
      UserGroupPage = pages.admin.userGroup;
      return meweHelper.loginAs('ADMIN');
    });
    after(() => {
      return meweHelper.logoutAs('ADMIN');
    });
    it('should be able to go to UserGroup page', () => {
      LeftMenu.userGroups.click();
      UserGroupPage.createUserGroupBtn.waitForVisible();
      UserGroupPage.createUserGroupBtn.click();
      UserGroupPage.saveBtn.waitForVisible();
    });
    it('should be able to input name and description then save successfully!', () => {
      UserGroupPage.inputUserGroupName.setValue(group.name);
      UserGroupPage.inputDescription.setValue(group.description);
      UserGroupPage.saveBtn.click();
      UserGroupPage.saveAlert.waitForVisible();
    });

    describe('adding users', () => {
      group.users.forEach((user) => {
        it(`should be able to add user: ${user.email}`, () => {
          meweHelper.multiSelect.chooseOption('#usergroup-user-search', user.email)
          UserGroupPage.addUsersBtn.waitForVisible(5000);
          UserGroupPage.addUsersBtn.click();
          browser.element(`header*=${user.email}`).waitForVisible(5000);
        });
      })
    });

    describe('adding checklists', () => {
      group.checklists.forEach((checklist) => {
        it(`should be able to add checklist: ${checklist.name}`, () => {
          meweHelper.multiSelect.chooseOption('#usergroup-checklist-search', checklist.name,'checklist')
          UserGroupPage.addChecklistsBtn.waitForVisible(5000);
          UserGroupPage.addChecklistsBtn.click();
          browser.element(`header*=${checklist.name}`).waitForVisible(5000);
        });
      })
    });

    describe('adding properties', () => {
      group.properties.forEach((property) => {
        it(`should be able to add property: ${property.name}`, () => {
          meweHelper.multiSelect.chooseOption('#usergroup-property-search', property.name)
          UserGroupPage.addPropertiesBtn.waitForVisible(5000);
          UserGroupPage.addPropertiesBtn.click();
          browser.element(`header*=${property.name}`).waitForVisible(5000);
        });
      })
    });
  })
}

describe('should be able to create a usergroup', () => {
  if (!reusedByAnotherSpec) run();
});

module.exports = run;
