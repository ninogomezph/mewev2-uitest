const LeftMenu = require('../pages/admin/leftMenu');

describe('SUB ADMIN', function() {

  before(() => {
    return meweHelper.loginAs('SUB_ADMIN');
  });
  after(() => {
    return meweHelper.logoutAs('ADMIN');
  });

  it('Settings menu should be hidden.', function() {
    expect(LeftMenu.navItemSettings.isVisible()).to.equal(false);
  });

});
