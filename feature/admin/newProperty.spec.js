
const pageElements = pages.admin;
const propertyPage = pageElements.propertyPage;

describe('Admin > Category > Property Page', function () {
  before(() => {
    meweHelper.loginAs('ADMIN');
    meweHelper.navigateTo([
      pageElements.leftMenu.navItemProperties,
      pageElements.propertyCategories.categories
    ])
    return;
  });
  after(() => meweHelper.logoutAs('ADMIN'));

  describe('Creating a new Property', function () {
    it('can press save', function () {
      meweHelper.waitFor(pageElements.propertiesPage.createPropertyBtn).click();

      propertyPage.propAddress.setValue('123 Filmore St');
      propertyPage.zip.setValue('1600');
      propertyPage.city.setValue('San Francisco');
      propertyPage.state.setValue('California');
      propertyPage.country.setValue('United States of Ranjeet');
      propertyPage.latitude.setValue('0.5');
      propertyPage.longitude.setValue('0.5');


      pageElements.propertyPage.savePropertyBtn.click();
      var alerts = meweHelper.waitFor(pageElements.alertHolder.alerts);

      expect(alerts.value.length).to.be.equal(1, 'There should only be 1 alert, and that is the save message');
      expect(alerts.getText()).to.be.equal('Building has been created');
    });

    it('changes persist after refresh', function () {
      browser.refresh();
      browser.pause(500)
      expect(propertyPage.propAddress.getValue()).to.be.equal('123 Filmore St');
      expect(propertyPage.zip.getValue()).to.be.equal('1600');
      expect(propertyPage.city.getValue()).to.be.equal('San Francisco');
      expect(propertyPage.state.getValue()).to.be.equal('California');
      expect(propertyPage.country.getValue()).to.be.equal('United States of Ranjeet');
      expect(propertyPage.latitude.getValue()).to.be.equal('0.5');
      expect(propertyPage.longitude.getValue()).to.be.equal('0.5');
    });
  });
});
