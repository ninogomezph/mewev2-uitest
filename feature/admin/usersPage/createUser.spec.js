const casual = require('casual');
const _ = require('lodash');
const reusedByAnotherSpec = !(module.parent.id.split('/').pop() === 'mocha.js');

function run(userToCreate) {
  if (reusedByAnotherSpec && !userToCreate) throw new Error('please define user to create');
  let LeftMenu;
  let UsersPage;
  let UserEditorPage;

  const password = casual.password;
  const user = _.defaultsDeep(userToCreate, {
    username: casual.username,
    email: casual.email,
    password,
    passwordCopy: password,
    firstName: casual.first_name,
    lastName: casual.last_name,
    role: 'AGENT',
  });

  before(() => {
    LeftMenu = pages.admin.leftMenu;
    UsersPage = pages.admin.usersPage;
    UserEditorPage = pages.admin.userPage;
    return meweHelper.loginAs('ADMIN');
  });
  after(() => {
    return meweHelper.logoutAs('ADMIN');
  });
  it('should be able to go to users tab', () => {
    LeftMenu.navItemUsers.click();
    UsersPage.createButton.waitForVisible();
  });
  it('should be able to open user editor page from "Create New User" button', () => {
    UsersPage.createButton.click();
    UserEditorPage.saveButton.waitForVisible();
  });
  it('should be able to input value in the form', () => {
    for(let key in user) {
      if (key !== 'role') UserEditorPage[key].setValue(user[key]);
    }
    UserEditorPage.selectRole(user.role);
  });
  it('should save and redirect the page', () => {
    UserEditorPage.saveButton.click();
    UserEditorPage.createdAlert.waitForVisible();
    var url = browser.getUrl();
    var id = +(url.split('/').pop());

    user.id = id;

    if (userToCreate) _.defaults(userToCreate, user);

    expect(isNaN(id)).to.be.false;
    expect(url).to.not.equal('http://localhost:1337/users/new');
  });

}

describe('should be able to create a user', () => {
  if (!reusedByAnotherSpec) run();
});

module.exports = run;
