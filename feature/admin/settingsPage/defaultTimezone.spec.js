'use strict';

describe('Admin Change Default Timezone', function() {
  let LeftMenu;
  let SettingsPage;
  before(() => {
    LeftMenu = pages.admin.leftMenu;
    SettingsPage = pages.admin.settingsPage;
    return meweHelper.loginAs('ADMIN');
  });
  after(() => {
    return meweHelper.logoutAs('ADMIN');
  });
  it('should go to settings page', () => {
    LeftMenu.navItemSettings.click();
    SettingsPage.GeneralTab.waitForVisible();
  });
  it('should go to General Tab', () => {
    SettingsPage.GeneralTab.click();
    browser.element('[ng-show="tabview == \'general\'"]').waitForVisible();
  });
  it('should select a timezone, save confirmation toast should appear', () => {
    browser.element('[input-model="timezones"] > span > button').click();
    browser.pause(500);
    browser.element('.multiSelectItem').click();
    browser.pause(500);
    expect(browser.getText('#alert-holder > ul > li')).to.equal('Default timezone updated');
  });
});
