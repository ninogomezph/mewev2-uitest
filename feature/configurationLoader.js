'use strict';
var uploadImage = process.env.UPLOAD_IMAGE === 'true';

var config = {
  MEWE_URL: process.env.MEWE_URL || 'http://localhost:1337', // docker0 || host || 'http://192.168.1.145:1337/'
  AGENT_EMAIL: process.env.AGENT_EMAIL || 'agent@v2.com',
  AGENT_PASSWORD: process.env.AGENT_PASSWORD || 'password',
  PROPERTY_NAME: process.env.PROPERTY_NAME || 'Testing Property',
  CHECKLIST_NAME: process.env.CHECKLIST_NAME || 'Testing Checklist',
  ADMIN_EMAIL: process.env.ADMIN_EMAIL || 'admin@v2.com',
  ADMIN_PASSWORD: process.env.ADMIN_PASSWORD || 'password',
  SUB_ADMIN_EMAIL: process.env.SUB_ADMIN_EMAIL || 'subadmin@v2.com',
  SUB_ADMIN_PASSWORD: process.env.SUB_ADMIN_PASSWORD || 'password',
  UPLOAD_IMAGE: uploadImage,
};

module.exports = config;
