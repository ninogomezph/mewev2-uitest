var config = require('./configurationLoader');
var LeftMenu = require('./pages/leftMenu');
var LoginPage = require('./pages/login_page');
var ChecklistCategoriesPage = require('./pages/checklist_categories_page.js');

console.log('Running admin checklist categories properties create');

describe('Admin checklist catagories properties search', function() {

  before(() => {
    return meweHelper.loginAs('ADMIN');
  });
  after(() => {
    return meweHelper.logoutAs('ADMIN');
  });

  it('goes to checklist page', function() {
    LeftMenu.navItemChecklist.click();
    ChecklistCategoriesPage.createCategoryBtn.waitForVisible();
    // PropertyCategoriesPage.searchBarInput.waitForVisible();
    assert(ChecklistCategoriesPage.createCategoryBtn.isVisible());
  });

  it('Create a new category then it will appear in the list', function() {
    ChecklistCategoriesPage.createdCategory.waitForVisible();

    var count = browser.elements('li.category.created').value.length;

    browser.pause(10000);
    var newCategoryName = 'Selenium Sample Checklist Category';
    ChecklistCategoriesPage.createCategoryBtn.click();
    ChecklistCategoriesPage.createForm.waitForVisible();
    ChecklistCategoriesPage.newCategoryInput.setValue(newCategoryName);
    ChecklistCategoriesPage.saveCategoryBtn.click();

    browser.pause(1000);
    var newCount = browser.elements('li.category.created').value.length;

    expect(newCount).is.equal(count + 1);
    assert(!ChecklistCategoriesPage.createForm.isVisible(), '.mewe-list.create-form should be hidden');
  });
});
