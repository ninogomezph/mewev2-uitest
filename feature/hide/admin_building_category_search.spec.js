var config = require('../configurationLoader');
var LeftMenu = require('../pages/leftMenu');
var LoginPage = require('../pages/login');
var PropertyPage = require('../pages/propertyPage');
var PropertyCategoriesPage = require('../pages/propertyCategories');


console.log('Running admin building categories properties search');

describe('Admin building catagories properties search', function() {
  it('permits login with correct credentials', function() {
    LoginPage.open(config.MEWE_URL);
    LoginPage.loginAs(config.ADMIN_EMAIL, config.ADMIN_PASSWORD, 'admin');
    assert.equal(browser.getTitle(), 'MeWe');
  });

  it('goes to properties page', function() {
    LeftMenu.navItemProperties.click();
    PropertyCategoriesPage.createCategoryBtn.waitForVisible();
    PropertyCategoriesPage.searchBarInput.waitForVisible();
    assert(PropertyCategoriesPage.createCategoryBtn.isVisible());
    assert(PropertyCategoriesPage.searchBarInput.isVisible());
  });

  it('Create a new category', function() {
    var newCategoryName = 'Selenium Sample Category';

    PropertyCategoriesPage.createCategoryBtn.click();
    PropertyCategoriesPage.newCategoryInput.waitForVisible();
    PropertyCategoriesPage.newCategoryInput.setValue(newCategoryName);
    PropertyCategoriesPage.saveCategoryBtn.click();

    browser.pause(1000);
    assert(PropertyCategoriesPage.createdCategory.isVisible());
  });

  it('Create a property', function() {
    PropertyCategoriesPage.createdCategory.click();
    PropertyPage.createPropertyBtn.waitForVisible(3000);
    PropertyPage.createPropertyBtn.click();

    PropertyPage.propName.setValue('Sample Property Name from Selenium');
    PropertyPage.propAddress.setValue('Sample Address from Selenium');
    PropertyPage.propAddress2.setValue('Sample Address 2 from Selenium');
    PropertyPage.savePropertyBtn.click();
    browser.pause(1000);
    LeftMenu.navItemProperties.click();
    PropertyCategoriesPage.createdCategory.waitForVisible();
    assert(PropertyCategoriesPage.createdCategory.isVisible());
  });

  it('Search the property', function() {
    PropertyCategoriesPage.searchBarInput.setValue('Sample Property Name');
    browser.pause(1000);
    assert(PropertyCategoriesPage.createdProperty.isVisible());
  });

  it('Open the property', function() {
    PropertyCategoriesPage.createdProperty.click();
    PropertyPage.propName.waitForVisible(2000);
    expect(PropertyPage.propName.getValue()).to.be.equal('Sample Property Name from Selenium');
  });

  it('Logs out', function() {
    LeftMenu.navItemLogout.click();
    assert.isOk(LoginPage.emailField.isVisible());
  });
});
