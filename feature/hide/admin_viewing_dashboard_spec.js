var config = require('./configurationLoader');
var LoginPage = require('./pages/login_page');
var LeftMenu = require('./pages/leftMenu');
var DashboardPage = require('./pages/dashboard_page');


console.log('Running admin_viewing_dashboard_spec.js');

describe('admin user viewing the dashboard', function() {
  it('logs in', function() {
    LoginPage.open(config.MEWE_URL);
    LoginPage.emailField.setValue(config.ADMIN_EMAIL);
    LoginPage.passwordField.setValue(config.ADMIN_PASSWORD);
    LoginPage.submit();

    assert.equal(browser.getTitle(), 'MeWe');
  });

  it('verifies that the Dashboard elements is present', function() {
    DashboardPage.inspectionGraph.waitForVisible(10000);
    assert(DashboardPage.inspectionGraph.isVisible());
    DashboardPage.inspectionTableTab.click();
    assert.isOk(DashboardPage.inspectionTable.isVisible());
  });

  it('logs out', function() {
    LeftMenu.navItemLogout.click();
    assert.isOk(LoginPage.emailField.isVisible());
  });
});
