var LoginPage = require('./pages/login_page');
var Menu = require('./pages/menu');
var NewInspectionModal = require('./pages/new_inspection_modal');
var InspectionPage = require('./pages/inspection_page');
var CriteriaPage = require('./pages/criteria_page');
var config = require('./configurationLoader');
var _ = require('lodash');

console.log('Running agent_spec.js');

describe('Inspections', function() {
  it('permits login with correct credentials', function() {
    console.log(config);
    LoginPage.open(config.MEWE_URL);
    LoginPage.loginAs(config.AGENT_EMAIL, config.AGENT_PASSWORD);
    if(LoginPage.loginAgent.isVisible()){
      LoginPage.logAgent.click();
    }
    assert.equal(browser.getTitle(), 'MeWe Agent');
  });

  it('creates impromptu inspection', function() {
    Menu.reveal();
    Menu.navItemNew.click();

    NewInspectionModal.openPropertyMenu();
    NewInspectionModal.propertySearchField.setValue(config.PROPERTY_NAME);
    var dropdownHighlightField = NewInspectionModal.inspectionDropdownHighlightedField;
    dropdownHighlightField.waitForVisible();
    browser.waitUntil(function() {
      return _.includes(dropdownHighlightField.getText(), config.PROPERTY_NAME);
    }, 5000, 'Expected property to show up in autofill search');
    browser.keys('Enter');

    expect(
      NewInspectionModal.inspectionDropdownValue.getText()
      ).to.contain(config.PROPERTY_NAME);
    NewInspectionModal.checklistMenu.selectByVisibleText(config.CHECKLIST_NAME);
    NewInspectionModal.createInspection.click();
  });

  it('loops through impromptu inspections and reviews answers', function() {
    InspectionPage.firstGroup.waitForVisible();
    InspectionPage.firstGroup.click();
    InspectionPage.firstCriteria.click();

    CriteriaPage.nextButton.waitForVisible();
    browser.pause(10000);
    while (CriteriaPage.hasAnotherQuestion()) {
      browser.pause(500);
      CriteriaPage.fillInWithRandomAnswers();
    }
  });

  it('show the previous question', function() {
    InspectionPage.firstGroup.waitForVisible();
    InspectionPage.firstGroup.click();
    browser.pause(500);

    if (CriteriaPage.nextButton.isVisible()) {
      CriteriaPage.nextButton.scroll();
      CriteriaPage.nextButton.click();
      browser.pause(10000);
      if (CriteriaPage.prevButton.isVisible()) {
        CriteriaPage.prevButton.waitForVisible();
        CriteriaPage.prevButton.click();
        if (CriteriaPage.backButton.isVisible) {
          CriteriaPage.backButton.click();
        }
        browser.pause(5000);
      }
    }
    assert(CriteriaPage.questionHeader.isVisible());
  });

  it('downloads report, generates PDF report and goes to the main home', function() {
    // MARK AS DONE
    InspectionPage.btnMarkAsDone.waitForVisible(10000);
    InspectionPage.btnMarkAsDone.click();
    InspectionPage.checklistDonePopup.waitForVisible();

    // CREATE REPORT
    InspectionPage.btnCreateReport.click();
    InspectionPage.btnDownloadReport.waitForVisible(20000);

    var oldWindowHandles = browser.windowHandles().value;
    InspectionPage.btnDownloadReport.click();
    var newWindowHandles = browser.windowHandles().value;
    // Verify that clicking download opens a new tab
    assert.equal(newWindowHandles.length, oldWindowHandles.length + 1);

    browser.pause(5000);
    browser.switchTab(oldWindowHandles);
    Menu.navHome.scroll();
    Menu.navHome.click();
    Menu.hamburger.waitForVisible(10000);
    assert(Menu.hamburger.isVisible());
  });

  it('logs out', function() {
    Menu.reveal();
    Menu.navItemLogout.click();
    assert.isOk(LoginPage.emailField.isVisible());
  });
});
