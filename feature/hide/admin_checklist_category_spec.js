var config = require('./configurationLoader');
var LoginPage = require('./pages/login_page');
var LeftMenu = require('./pages/leftMenu');
var ChecklistPage = require('./pages/checklist_page');



console.log('admin checklist editor');
describe('Agent editing the checklist', function() {
  it('logs into the admin site',function () {
    LoginPage.open(config.MEWE_URL);
    LoginPage.emailField.setValue(config.ADMIN_EMAIL);
    LoginPage.passwordField.setValue(config.ADMIN_PASSWORD);
    LoginPage.submit();

    assert.equal(browser.getTitle(), 'MeWe');
  });

  it('goes to the checklist page',function(){
    LeftMenu.navItemChecklist.click();
    ChecklistPage.listOfChecklist.waitForVisible(5000);
    assert(ChecklistPage.listOfChecklist.isVisible());
  });

  it('edit the checklist category',function() {
    browser.pause(500);
    ChecklistPage.editCategory.click();
    ChecklistPage.editPlaceholder.setValue('100Q1');
    ChecklistPage.categorySaveBtn.click();
    browser.pause(500);

    assert(ChecklistPage.addChecklistBtn.isVisible());
  });

  it('cancels the checklist confirmation message',function() {
    ChecklistPage.categoryDeleteBtn.click();
    ChecklistPage.noButton.waitForVisible();
    ChecklistPage.noButton.click();
    assert(ChecklistPage.addChecklistBtn.isVisible());
  });

  it('clicks on the category and redirect to the checklist page',function () {
    ChecklistPage.categoryItem.click(); 
    browser.pause(5000);
    assert(ChecklistPage.createChecklistButton.isVisible());
  });
  it ('deletes the checklist category', function () {
    LeftMenu.navItemChecklist.click();
    ChecklistPage.categoryDeleteBtn.waitForVisible();
    ChecklistPage.categoryDeleteBtn.click();
    ChecklistPage.yesButton.waitForVisible();
    ChecklistPage.yesButton.click();
    assert(ChecklistPage.addChecklistBtn.isVisible());
  });
});
