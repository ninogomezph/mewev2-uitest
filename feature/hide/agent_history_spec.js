var LoginPage = require('./pages/login_page');
var Menu = require('./pages/menu');
var Tab = require('./pages/tab_page');
var config = require('./configurationLoader');


describe('Agent checks on the building history', function() {
  it('permits login with correct credentials', function() {
    LoginPage.open(config.MEWE_URL);
    LoginPage.loginAs(config.AGENT_EMAIL, config.AGENT_PASSWORD);
    assert.equal(browser.getTitle(), 'MeWe Agent');
  });

  it('goes to OPEN tab and checks on the building history', function() {
    Tab.openTab.click();
    Tab.propertyName.waitForVisible();
    Tab.propertyName.click();
    assert(Tab.coinspectTitle.isVisible());
  });

  it('loads the history', function() {
    Tab.loadHistory.scroll();
    Tab.loadHistory.click();
    browser.scroll(0, 500);
    browser.pause(500);
    Tab.coinspectTitle.waitForVisible();
    assert(Tab.coinspectTitle.isVisible());
  });

  it('hides the history', function() {
    Tab.hideHistory.waitForVisible(3000);
    Tab.hideHistory.click();
    assert(Tab.coinspectTitle.isVisible());
  });

  it('logs out', function() {
    Menu.reveal();
    Menu.navItemLogout.click();
    assert.isOk(LoginPage.emailField.isVisible());
  });
});
