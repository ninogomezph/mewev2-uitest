var config = require('./configurationLoader');
var LoginPage = require('./pages/login_page');
var LeftMenu = require('./pages/leftMenu');
var ChecklistPage = require('./pages/checklist_page');
var EmailPage = require('./pages/checklist_email_page');



console.log('Running Checklist Email notification - Teardown');

describe('Checklist Email Notification- Teardown', function() {
  // body...
  it('logs into the admin site', function() {
    LoginPage.open(config.MEWE_URL);
    LoginPage.emailField.setValue(config.ADMIN_EMAIL);
    LoginPage.passwordField.setValue(config.ADMIN_PASSWORD);
    LoginPage.submit();

    assert.equal(browser.getTitle(), 'MeWe');
  });

  it('goes to the checklist page', function() {
    LeftMenu.navItemChecklist.click();
    ChecklistPage.listOfChecklist.waitForVisible(5000);
    assert(ChecklistPage.listOfChecklist.isVisible());
  });

  it('clicks on DOTA -2  checklist category', function() {
    ChecklistPage.dotaTitle.scroll();
    ChecklistPage.dotaTitle.click();
    ChecklistPage.checklistCategoryTxt.waitForVisible(5000);
    browser.pause(500);
    assert(ChecklistPage.checklistCategoryTxt.isVisible());
  });

  it('clicks on a DOTA 2 - copy checklist', function() {
    ChecklistPage.dota2checklist.waitForVisible(10000);
    ChecklistPage.dota2checklist.click();
    assert(ChecklistPage.checklistCategoryTxt.isVisible());
  });

  it('goes to Email Notification section', function() {
    EmailPage.questionHeader.waitForVisible();
    EmailPage.emailDelete.scroll();
    assert(EmailPage.questionHeader.isVisible());
  });
  

  it('deletes the email set on the email notification section',function() {
    if(EmailPage.emailDelete.isVisible()){
       EmailPage.emailDelete.scroll();
       EmailPage.emailDelete.click();
       console.log('deleted!');
     browser.pause(500);
    }
    assert(ChecklistPage.checklistCategoryTxt.isVisible());
  });
  
  it('logs out', function() {
    LeftMenu.navItemLogout.click();
    LoginPage.loginLogo.waitForVisible();
    assert.isOk(LoginPage.loginLogo.isVisible());
  });

  
});
