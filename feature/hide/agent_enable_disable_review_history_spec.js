const helpers = require('./helpers');
const config = require('./configurationLoader');
const LeftMenu = require('./pages/leftMenu');
const LoginPage = require('./pages/login_page');
const SettingsPage = require('./pages/settings_page');
const AgentMenu = require('./pages/menu');

console.log('Running admin_toggle_login_as_agent_spec.js');

describe('Agent Review History enable/disable', function() {
  before(() => {
    return meweHelper.loginAs('ADMIN');
  });
  after(() => {
    return meweHelper.logoutAs('AGENT');
  });
  it('should allow Admin to disable Review History in Settings > Mobile Tab', () => {
    LeftMenu.navItemSettings.click();
    SettingsPage.MobileTab.click();
    browser.timeoutsImplicitWait(3);
    SettingsPage.toggleReviewHistoryOff.click();
  });
  it('should be able to switch to agent view and not see the Review History button in menu', () => {
    LeftMenu.navItemSwitchView.click();
    browser.timeoutsImplicitWait(3);
    AgentMenu.reveal();
    expect(AgentMenu.navItemReviewHistory.isVisible()).to.be.false; // its disabled
  });
  it('should be able to switch to admin and enable Review History', () => {
    AgentMenu.navItemSwitchView.click();
    browser.timeoutsImplicitWait(3);
    LeftMenu.navItemSettings.click();
    SettingsPage.MobileTab.click();
    browser.timeoutsImplicitWait(3);
    SettingsPage.toggleReviewHistoryOn.click();
  });
  it('should be able to switch to agent view and see the Review History button in menu', () => {
    LeftMenu.navItemSwitchView.click();
    browser.timeoutsImplicitWait(3);
    AgentMenu.reveal();
    expect(AgentMenu.navItemReviewHistory.isVisible()).to.be.true; // its enabled again
  });
});
