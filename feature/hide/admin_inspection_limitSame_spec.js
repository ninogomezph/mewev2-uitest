var SettingsPage = require('./pages/settings_page');
var config = require('./configurationLoader');
var LoginPage = require('./pages/login_page');
var LeftMenu = require('./pages/leftMenu');
var MobilePage = require('./pages/mobile_page');
var Tab = require('./pages/tab_page');
var Menu = require('./pages/menu');
var Search = require('./pages/search_page');

console.log('Running admin mobile setting - inspection search limitation- SAME USER');

describe('Setting inspection search limitation to SAME USER', function(){
console.log('Running admin mobile setting -inspection search limitation');

describe('Setting the mobile setting for inspection search limitation', function() {

  it('permits login with correct credentials', function() {
      LoginPage.open(config.MEWE_URL);
      LoginPage.loginAs(config.ADMIN_EMAIL, config.ADMIN_PASSWORD);
      assert.equal(browser.getTitle(), 'MeWe');
  });

  it('goes to settings page', function() {
      LeftMenu.navItemSettings.click();
      SettingsPage.txtSettings.waitForVisible();
      assert(SettingsPage.txtSettings.isVisible());
  });

  it('goes to Mobile Tab', function() {
      browser.windowHandleSize({ width: 1300, height: 768 });
      MobilePage.mobileTab.waitForVisible();
      MobilePage.mobileTab.click();
      MobilePage.listOfToggles.waitForVisible();
      browser.pause(5000);
      assert(MobilePage.listOfToggles.isVisible());
  });

  it('switches inspection search limitation -"Same User"', function() {
      MobilePage.inspectionSwitchLimitSame();
      browser.pause(500);
      MobilePage.inspectionLimitValue.getValue();
      assert.equal(MobilePage.inspectionLimitValue.getValue(),'same_user');
  });

  it('logs out', function() {
      LeftMenu.navItemLogout.click();
      assert.isOk(LoginPage.emailField.isVisible());
  });


  it('logs in as agent', function() {
    LoginPage.open(config.MEWE_URL);
    LoginPage.loginAs(config.AGENT_EMAIL, config.AGENT_PASSWORD);
    assert(browser.getTitle(), 'MeWe Agent');
  });

  it('chooses "Search" over the menu', function() {
    Menu.reveal();
    Menu.navItemSearch.click();
    Search.searchTxt.waitForVisible(2000);
    assert(Search.searchTxt.isVisible());
  });

  it('shows the Search modal and inputs value',function() {
   if(Search.searchField.isVisible()){
    Search.searchField.setValue('hehe');

      if(Search.btnContinue.isVisible()){
        Search.btnContinue.click();
        Search.searchItem.waitForVisible(3000);  
      } else if (Search.btnBack.isVisible()){
        Search.btnBack.click();
      }
   }
   assert.isFalse(Search.searchItem.isVisible());
  });

  // it('goes to OPEN tab and checks on the building history', function() {
  //   Tab.openTab.click();
  //   Tab.propertyName.waitForVisible(3000);
  //   Tab.propertyName.click();
  //   Tab.loadHistory.click();
  //   Tab.listHistory.waitForVisible(5000);
  //   Tab.reviewButton.scroll();
  //   browser.pause(5000);
  //   assert(Tab.listHistory.isVisible());
  // });

  it('it reviews an inspection under building history', function() {
    Tab.reviewButton.click();
    Tab.checklistReview.waitForVisible(25000);
    browser.pause(500);
    assert(Tab.checklistReview.isVisible());
    });  


  it('logs out', function() {
    Menu.reveal();
    Menu.navItemLogout.click();
    assert.isOk(LoginPage.emailField.isVisible());
    });
  });
});
