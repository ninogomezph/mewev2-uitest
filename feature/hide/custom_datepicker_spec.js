const helpers = require('./helpers');
const config = require('./configurationLoader');
const LeftMenu = require('./pages/leftMenu');
const ChecklistPage = require('./pages/checklist_page');
const AgentMenu = require('./pages/menu');
const path = require('path');
const zipFileToUpload = path.join(__dirname, "checklist", "Date.zip");
console.log('Running custom_datepicker_spec.js');

describe('Cross Browser Date Picker (custom)', function() {
  it('Login as admin to create checklist', function() {
    return helpers.loginAs('ADMIN');
  });

  it('Should show the date picker when dashboard start date is clicked.', function() {
    browser.element('input[ng-model="dates.start"]').click();
    browser.element(".datepicker.datepicker-dropdown.dropdown-menu").waitForVisible(5000);
    return assert(browser.element(".datepicker.datepicker-dropdown.dropdown-menu").isVisible());
  });

  it('Create Checklist named "Date"', function(){
    return helpers.createChecklist("Test Datepicker", "Date", "Date.zip");
  });

  it('Create Property named "Datepicker"', function(){
    return helpers.createProperty({name: "Datepicker", category: "Test"});
  });

  it('Create User Group named "Group Date"', function(){
    return helpers.createUserGroup({name: "Datepicker", checklist: "Date", property: "Datepicker", user: config.AGENT_EMAIL});
  });

  it('Logs out and login as Agent', function() {
    LeftMenu.navItemLogout.click();
    browser.timeoutsImplicitWait(10);
    return meweHelper.loginAs('AGENT');
  });

  it('Create an impromptu inspection', function() {
    AgentMenu.reveal();
    browser.pause(1000);
    AgentMenu.navItemNew.click()
    browser.pause(1000);
    browser.element(".select2-selection__placeholder").moveToObject();
    browser.element(".select2-selection__placeholder").click()
    browser.pause(1000);

    helpers.betterSelect.chooseOption("Datepicker");
    browser.pause(1000);
    browser.element('select[ng-model="inspection.checklistId"]').selectByVisibleText("Date");
    browser.pause(1000);
    browser.element("button.button-impromptu-expand=START").click();
    browser.element("#mark-as-done").waitForVisible(5000);
    return assert(browser.element("#mark-as-done").isVisible());
  });

  it('Should show the date picker when input field is clicked.', function() {
    browser.element("div=Group1").click();
    browser.element('aside=1.1').waitForVisible(5000);
    browser.element('aside=1.1').click();
    browser.element('input[any-type="date"]').waitForVisible(5000);
    browser.element('input[any-type="date"]').click();
    browser.element(".datepicker.datepicker-dropdown.dropdown-menu").waitForVisible(5000);
    return assert(browser.element(".datepicker.datepicker-dropdown.dropdown-menu").isVisible());
  });

});
