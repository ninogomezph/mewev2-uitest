var config = require('./configurationLoader');
var LoginPage = require('./pages/login_page');
var LeftMenu = require('./pages/leftMenu');
var AssignmentPage = require('./pages/assignment_page');

console.log('Running admin_deleting_inspection_spec.js');

describe('admin deleting inspections', function() {
  it('logs in', function() {
    LoginPage.open(config.MEWE_URL);
    LoginPage.emailField.setValue(config.ADMIN_EMAIL);
    LoginPage.passwordField.setValue(config.ADMIN_PASSWORD);
    if(LoginPage.loginAdmin.isVisible()){
      LoginPage.loginAdmin.click();
    }
    LoginPage.submit();

    assert.equal(browser.getTitle(), 'MeWe');
  });


  it('goes to "Assignment Page"', function() {
    LeftMenu.navItemAssignments.click();
    assert(AssignmentPage.txtInspectionTab.isVisible());
  });

  it('it loads the  Assignment Page', function() {
    AssignmentPage.txtInspectionTab.click();
    AssignmentPage.listHeader.isVisible();
    assert(AssignmentPage.txtInspectionTab.isVisible());
  });

  it('it deletes the inspection', function() {
    browser.windowHandleSize({ width: 1300, height: 768 });
    if (AssignmentPage.assignmentItem !== null) {
      AssignmentPage.assignmentItem.waitForVisible(5000);
      var deletedTimeStamp = AssignmentPage.firstTimeStampText;
      AssignmentPage.deleteFirstInspection();
      AssignmentPage.btnYes.click();
      browser.waitUntil(function() {
        return AssignmentPage.firstTimeStampText !== deletedTimeStamp;
      }, 8000, 'Did not detect a change in the first timestamp in this table - did the assignment delete correctly?');
      var nextTimeStamp = AssignmentPage.firstTimeStampText;
      assert.notEqual(deletedTimeStamp, nextTimeStamp, 'inspection deleted successfully');
    }
  });

  it('logs out', function() {
    LeftMenu.navItemLogout.click();
    assert.isOk(LoginPage.emailField.isVisible());
  });
});
