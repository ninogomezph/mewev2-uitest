var config = require('./configurationLoader');
var LoginPage = require('./pages/login_page');
var LeftMenu = require('./pages/leftMenu');
var SettingsPage = require('./pages/settings_page');

console.log('Running admin_whitelabeling_navlogo_spec.js');


describe('admin uploading image logo', function() {
  it('logs in', function() {
    LoginPage.open(config.MEWE_URL);
    LoginPage.emailField.setValue(config.ADMIN_EMAIL);
    LoginPage.passwordField.setValue(config.ADMIN_PASSWORD);
    LoginPage.submit();

    assert.equal(browser.getTitle(), 'MeWe');
  });

  it('goes to "Settings Page"', function() {
    LeftMenu.navItemSettings.click();
    assert(SettingsPage.txtSettings.isVisible());
  });

  it('clicks the "Logos" tab', function() {
    SettingsPage.logosTab.click();
    assert(SettingsPage.logosTab.isVisible());
  });

  it('goes to the Navigation Bar', function() {
    SettingsPage.navBarOption.click();
    assert(SettingsPage.logosTab.isVisible());
  });

  it('uploads an image as navigation logo ', function() {
    if (SettingsPage.btnFile.isExisting()) {
      SettingsPage.btnFile.click();
      SettingsPage.uploadsLogo();
      browser.pause(500);
      SettingsPage.uploadedLogo.waitForVisible(6000);
    } else if (SettingsPage.uploadedLogo.isVisible()) {
      SettingsPage.btnReplace.click();
      browser.pause(500);
      SettingsPage.txtSettings.waitForVisible(6000);
    }
    assert(SettingsPage.txtSettings.isVisible());
  });

  it('logs out', function() {
    LeftMenu.navItemLogout.click();
    assert.isOk(LoginPage.emailField.isVisible());
  });

  it('logs in again ', function() {
    LoginPage.open(config.MEWE_URL);
    LoginPage.emailField.setValue(config.ADMIN_EMAIL);
    LoginPage.passwordField.setValue(config.ADMIN_PASSWORD);
    LoginPage.submit();

    assert.equal(browser.getTitle(), 'MeWe');
  });
});
