var LoginPage = require('./pages/login_page');
var Menu = require('./pages/menu');
var Search = require('./pages/search_page');
var config = require('./configurationLoader');
var _ = require('lodash');

console.log('Running inspection_search_spec.js');

describe('Inspections', function() {
  it('permits login with correct credentials', function() {
    LoginPage.open(config.MEWE_URL);
    LoginPage.loginAs(config.AGENT_EMAIL, config.AGENT_PASSWORD);
    assert.equal(browser.getTitle(), 'MeWe Agent');
  });

  it('goes to search modal', function() {
    Menu.reveal();
    Menu.navItemSearch.click();
    Search.searchTxt.waitForVisible(10000);
    assert(Search.searchTxt.isVisible());
  });

  it('lets you input a value and queries result', function() {
    Search.searchField.setValue('comment');
    Search.btnContinue.click();
    Search.btnBack.waitForVisible(3000);
    assert(Search.btnBack.isVisible());
  });


  it('lets you select a search item result', function() {
    if (Search.searchItem.isVisible()) {
      Search.searchItem.click();
      Search.btnCreatePDF.waitForVisible(3000);
      assert(Search.btnCreatePDF.isVisible());
    } else if (Search.btnBack.isVisible()) {
      Search.btnBack.click();
      Search.searchField.waitForVisible(5000);
      assert(Search.btnContinue.isVisible());
    } else {
      Menu.reveal();
      Menu.navItemLogout.click();
      assert.isOk(LoginPage.emailField.isVisible());
    }
  });

  it('creates and downloads PDF report', function() {
    if (Search.btnCreatePDF.isVisible()) {
      Search.btnCreatePDF.click();
      Search.btnDownloadPDF.waitForVisible(5000);
      var oldWindowHandles = browser.windowHandles().value;
      Search.btnDownloadPDF.click();
      var newWindowHandles = browser.windowHandles().value;
      assert.equal(newWindowHandles.length, oldWindowHandles.length + 1);
    } else if (Search.btnCreatePDF.isVisible()) {
      Search.btnBack.click();
      Search.searchField.waitForVisible(5000);
      assert(Search.btnContinue.isVisible());
    } else {
      Menu.reveal();
      Menu.navItemLogout.click();
      assert.isOk(LoginPage.emailField.isVisible());
    }
  });
});
