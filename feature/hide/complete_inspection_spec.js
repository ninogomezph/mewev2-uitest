var LoginPage = require('./pages/login_page');
var Menu = require('./pages/menu');
var LeftMenu = require('./pages/leftMenu');
var NewInspectionModal = require('./pages/new_inspection_modal');
var InspectionPage = require('./pages/inspection_page');
var CriteriaPage = require('./pages/criteria_page');
var AssignmentPage = require('./pages/assignment_page');
var config = require('./configurationLoader');
var _ = require('lodash');

console.log('Running complete_inspection_spec.js');

describe('Complete Walkthrough - impromptu Inspections', function() {
  it('agent permits login with correct credentials', function() {
    LoginPage.open(config.MEWE_URL);
    LoginPage.loginAs(config.AGENT_EMAIL, config.AGENT_PASSWORD);
    assert.equal(browser.getTitle(), 'MeWe Agent');
  });

  it('creates impromptu inspection', function() {
    Menu.reveal();
    Menu.navItemNew.click();

    NewInspectionModal.openPropertyMenu();
    NewInspectionModal.propertySearchField.setValue(config.PROPERTY_NAME);
    var dropdownHighlightField = NewInspectionModal.inspectionDropdownHighlightedField;
    dropdownHighlightField.waitForVisible();
    browser.waitUntil(function() {
      return _.includes(dropdownHighlightField.getText(), config.PROPERTY_NAME);
    }, 5000, 'Expected property to show up in autofill search')
    browser.keys('Enter');

    expect(
      NewInspectionModal.inspectionDropdownValue.getText()
      ).to.contain(config.PROPERTY_NAME);
    NewInspectionModal.checklistMenu.selectByVisibleText(config.CHECKLIST_NAME);
    NewInspectionModal.createInspection.click();
  });

  it('loops through impromptu inspections and marks Yes', function() {
    InspectionPage.firstGroup.waitForVisible();
    InspectionPage.firstGroup.click();
    InspectionPage.firstCriteria.click();

    CriteriaPage.nextButton.waitForVisible();
    while (CriteriaPage.hasAnotherQuestion()) {
      browser.pause(500);
      CriteriaPage.fillInWithRandomAnswers();
    }
  });


  it('downloads and generates PDF report', function() {
    // MARK AS DONE
    InspectionPage.btnMarkAsDone.waitForVisible();
    InspectionPage.btnMarkAsDone.click();
    InspectionPage.checklistDonePopup.waitForVisible(25000);

    // CREATE REPORT
    InspectionPage.btnCreateReport.click();
    InspectionPage.btnDownloadReport.waitForVisible(25000);

    var oldWindowHandles = browser.windowHandles().value;
    InspectionPage.btnDownloadReport.click();
    var newWindowHandles = browser.windowHandles().value;

    // Verify that clicking download opens a new tab
    assert.equal(newWindowHandles.length, oldWindowHandles.length + 1);
  });

  it('logs out', function() {
    Menu.reveal();
    Menu.navItemLogout.click();

    assert.isOk(LoginPage.emailField.isVisible());
  });


// ADMIN SIDE 
  it('logs in as admin', function() {
    LoginPage.open(config.MEWE_URL);
    LoginPage.emailField.setValue(config.ADMIN_EMAIL);
    LoginPage.passwordField.setValue(config.ADMIN_PASSWORD);
    LoginPage.submit();

    assert.equal(browser.getTitle(), 'MeWe');
  });


  it('goes to "Assignment Page"', function() {
    LeftMenu.navItemAssignments.click();
    assert(AssignmentPage.txtInspectionTab.isVisible());
  });

  it('it loads the  Assignment Page', function() {
    AssignmentPage.txtInspectionTab.click();
    AssignmentPage.listHeader.isVisible();
    assert(AssignmentPage.txtInspectionTab.isVisible());
  });

  it('it deletes the inspection', function() {
    browser.windowHandleSize({ width: 1300, height: 768 });
    if (AssignmentPage.assignmentItem !== null) {
      AssignmentPage.assignmentItem.waitForVisible(15000);
      var deletedTimeStamp = AssignmentPage.firstTimeStampText;
      AssignmentPage.deleteFirstInspection();
      AssignmentPage.btnYes.click();
      browser.waitUntil(function() {
        return AssignmentPage.firstTimeStampText !== deletedTimeStamp;
      }, 8000, 'Did not detect a change in the first timestamp in this table - did the assignment delete correctly?');
      var nextTimeStamp = AssignmentPage.firstTimeStampText;
      assert.notEqual(deletedTimeStamp, nextTimeStamp, 'inspection deleted successfully');
    }
  });

  it('logs out', function() {
    LeftMenu.navItemLogout.click();
    assert.isOk(LoginPage.emailField.isVisible());
  });
});
