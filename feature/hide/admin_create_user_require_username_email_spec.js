'use strict';
const config = require('./configurationLoader');
const LeftMenu = require('./pages/leftMenu');
const UserPage = require('./pages/user_page');
const UsersPage = require('./pages/users_page');

console.log('Running admin_create_user_require_username.js');
/*
default is
user:require-email = true
user:require-email = true
*/
describe('Admin Create User', function () {
  let username;
  before(() => {
    username = meweHelper.randomString();
    return meweHelper.loginAs('ADMIN');
  });
  after(() => {
    return meweHelper.logoutAs('ADMIN');
  });
  it('go to USERS page', () => {
    LeftMenu.navItemUsers.click();
    browser.timeoutsImplicitWait(3);
    assert(UsersPage.createButton.isVisible());
  });
  it('go to USER editor page via create new button', () => {
    UsersPage.createButton.click();
    browser.timeoutsImplicitWait(3);
    assert(UserPage.saveButton.isVisible());
  });
  it('save should alert user to provide profile firstName and lastName', () => {
    browser.timeoutsImplicitWait(3);
    UserPage.saveButton.click();
    UserPage.profileAlert.waitForVisible();
    UserPage.firstName.setValue('First Name');
    UserPage.lastName.setValue('Last Name');
  });
  it('save should alert user to provide USERNAME', () => {
    UserPage.saveButton.click();
    UserPage.usernameAlert.waitForVisible();
    UserPage.username.setValue(`username-${username}`);
  });
  it('save should alert user to provide PASSWORD', () => {
    UserPage.saveButton.click();
    UserPage.passwordAlert.waitForVisible();
    UserPage.password.setValue('password123');
  });
  it('save should alert user to provide PASSWORD COPY', () => {
    UserPage.saveButton.click();
    UserPage.passwordAlert.waitForVisible();
    UserPage.passwordCopy.setValue('password123');
  });
  it('save should alert user to provide EMAIL', () => {
    UserPage.saveButton.click();
    UserPage.emailAlert.waitForVisible();
  });
  it('save should alert user to provide VALID EMAIL and EMAIL', () => {
    UserPage.email.setValue('invalidEmail');
    UserPage.saveButton.click();
    UserPage.emailAlert.waitForVisible();
  });
  it('create user', () => {
    UserPage.email.setValue(`username-${username}@test.com`);
    UserPage.saveButton.click();
    UserPage.createdAlert.waitForVisible();
  });
});
