const helpers = require('./helpers');
const config = require('./configurationLoader');
const LeftMenu = require('./pages/leftMenu');
const LoginPage = require('./pages/login_page');
const SettingsPage = require('./pages/settings_page');

console.log('Running admin_toggle_login_as_agent_spec.js');

describe('Admin Toggle Login as Agent', function() {
  before(() => {
    return meweHelper.loginAs('ADMIN');
  });
  after(() => {
    LeftMenu.navItemSettings.click();
    SettingsPage.AuthTab.click();
    browser.timeoutsImplicitWait(3);
    SettingsPage.toggleAllowLoginAsAgentOn.click();
    return meweHelper.logoutAs('ADMIN');
  });
  it('should be able to set Allow Login as Agent toggle to off', () => {
    LeftMenu.navItemSettings.click();
    SettingsPage.AuthTab.click();
    browser.timeoutsImplicitWait(3);
    SettingsPage.toggleAllowLoginAsAgentOff.click();
  });
  it('should be able to logout', () => {
    meweHelper.logoutAs('ADMIN');
    browser.timeoutsImplicitWait(3);
    expect(LoginPage.loginAgent.isVisible()).to.be.false;
    expect(LoginPage.loginAdmin.isVisible()).to.be.false;
    return assert.isOk(LoginPage.emailField.isVisible());
  });
  it('should be able to login again, but "login as" option should be gone as well as "Agent Mode"', () => {
    meweHelper.loginAs('ADMIN');
    browser.timeoutsImplicitWait(3);
    expect(LeftMenu.navItemSwitchView.isVisible()).to.be.false;
  });
});
