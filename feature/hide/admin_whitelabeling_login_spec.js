var config = require('./configurationLoader');
var LoginPage = require('./pages/login_page');
var LeftMenu = require('./pages/leftMenu');
var SettingsPage = require('./pages/settings_page');

console.log('Running admin_whitelabeling_login_spec.js');

describe('admin uploading image logo', function() {
  it('logs in', function() {
    LoginPage.open(config.MEWE_URL);
    LoginPage.emailField.setValue(config.ADMIN_EMAIL);
    LoginPage.passwordField.setValue(config.ADMIN_PASSWORD);
    LoginPage.submit();

    assert.equal(browser.getTitle(), 'MeWe');
  });

  it('goes to "Settings Page"', function() {
    LeftMenu.navItemSettings.click();
    assert(SettingsPage.txtSettings.isVisible());
  });

  it('clicks the "Logos" tab', function() {
    SettingsPage.logosTab.click();
    assert(SettingsPage.logosTab.isVisible());
  });

  it('goes to the "Login Screen', function() {
    SettingsPage.loginScreenOption.click();
    assert(SettingsPage.headingUpload.isVisible());
  });

  it('uploads an image as login screen', function() {
    if (SettingsPage.btnFile.isExisting()) {
      SettingsPage.btnFile.doDoubleClick();
      SettingsPage.uploadsLogo();
      browser.pause(500);
      SettingsPage.uploadedLogo.waitForVisible();
      assert(SettingsPage.logosTab.isVisible());
    } else if (SettingsPage.uploadedLogo.isExisting()) {
      browser.doDoubleClick('SettingsPage.btnReplace');
      browser.pause(500);
      assert(SettingsPage.logosTab.isVisible());
    }
  });

  it('logs out', function() {
    LeftMenu.navItemLogout.click();
    LoginPage.loginLogo.waitForVisible();
    assert.isOk(LoginPage.loginLogo.isVisible());
  });
});
