var config = require('./configurationLoader');
var LoginPage = require('./pages/login_page');
var LeftMenu = require('./pages/leftMenu');
var SettingsPage = require('./pages/settings_page');

console.log('Running admin_setting_email_spec.js');

describe('admin creating new email', function() {
  it('logs in', function() {
    LoginPage.open(config.MEWE_URL);
    LoginPage.emailField.setValue(config.ADMIN_EMAIL);
    LoginPage.passwordField.setValue(config.ADMIN_PASSWORD);
    LoginPage.submit();

    assert.equal(browser.getTitle(), 'MeWe');
  });

  it('goes to "Settings Page"', function() {
    LeftMenu.navItemSettings.click();
    assert(SettingsPage.txtSettings.isVisible());
  });

  it('clicks the "Emails" tab', function() {
    SettingsPage.emailsTab.click();
    SettingsPage.createEmail.waitForVisible(3000);
    SettingsPage.createEmail.click();
    assert(SettingsPage.emailsTab.isVisible());
  });

  it('adds input values to the new email', function() {
    browser.windowHandleSize({ width: 1300, height: 768 });
    SettingsPage.emailTo.setValue(config.TO_EMAIL);
    SettingsPage.emailAdd.click();
    SettingsPage.emailCc.setValue(config.CC_EMAIL);
    SettingsPage.emailccAdd.click();
    SettingsPage.emailSubject.setValue('Inspection Started Notification');  
    SettingsPage.emailNotif.click();
    SettingsPage.emailNotif.selectByValue('started');  
    assert(SettingsPage.emailsTab.isVisible());
  });

  it('sets the email content', function() {
    SettingsPage.emailContent.click();
    SettingsPage.emailContent.setValue('The inspection has been started. Please check on your admit site.');
    SettingsPage.emailSave.click();
  });

  it('logs out', function() {
    LeftMenu.navItemLogout.click();
    LoginPage.loginLogo.waitForVisible();
    assert.isOk(LoginPage.loginLogo.isVisible());
  });



});
