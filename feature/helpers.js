'use strict';
const loginAs = require('./helpers/loginAs');
const logoutAs = require('./helpers/logoutAs');
const createChecklist = require('./helpers/createChecklist');
const createProperty = require('./helpers/createProperty');
const createUserGroup = require('./helpers/createUserGroup');
const takeScreenshot = require('./helpers/takeScreenshot');
const betterSelect = require('./helpers/betterSelect');
const multiSelect = require('./helpers/multiselect');
const navigateTo = require('./helpers/navigateTo');

function randomString(count) {
  return Math.random().toString(36).substring(count || 7);
}

function waitFor(e,time){
  e.waitForExist(time || 500)
  return e
}

module.exports = {
  loginAs,
  logoutAs,
  createChecklist,
  createProperty,
  createUserGroup,
  betterSelect,
  takeScreenshot,
  randomString,
  navigateTo,
  waitFor,
  multiSelect,
};
